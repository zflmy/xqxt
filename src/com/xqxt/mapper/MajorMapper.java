package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Major;

public interface MajorMapper {

	void insMajor(Major major);
	
	List<Major> selMajorByAcademy(String academy_no);
	
	List<Major> selALLMajor();
}
