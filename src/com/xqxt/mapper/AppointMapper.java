package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Appoint;

public interface AppointMapper {

	void insAppoint(Appoint appoint);
	
	List<Appoint> selAppoint();
	
	void updAppoint(Appoint appoint);
	
	Appoint selOneAppoint(String c_email);
}
