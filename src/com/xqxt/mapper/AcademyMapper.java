package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Academy;

public interface AcademyMapper {

	void insAcademy(Academy academy);
	
	List<Academy> selAcademy();
	
}
