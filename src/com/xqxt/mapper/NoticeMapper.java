package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Notice;

public interface NoticeMapper {

	void insNotice(Notice notice);
	
	List<Notice> selNotice(String sendto);
	
}
