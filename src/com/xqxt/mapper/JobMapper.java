package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.JOC;

public interface JobMapper {

	List<JOC> selJob(String c_email, String occupate_no);
	
	List<JOC> selJobByNo(String c_email);
	
	JOC selJocByNo(String c_email, String job_no);
	
}
