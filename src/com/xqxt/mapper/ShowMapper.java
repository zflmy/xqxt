package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Count;

public interface ShowMapper {

	List<Count> selWhole();
	
	List<Count> selByAcademy(String academy_no);
	
	List<Count> selByMajor(String major_no);
	
	List<Count> selAcademys();
	
	List<Count> selMajors(String academy_no);
	
}
