package com.xqxt.mapper;

import java.util.List;

import com.xqxt.entity.Company;
import com.xqxt.entity.JC;
import com.xqxt.entity.Occupate;

public interface CompanyMapper {

	Company selCompanyByNo(String c_email);
	
	List<Occupate> selOccupateByNo(String c_email);
	
	List<Company> selCompany();

	List<JC> selJobByCompany(JC jc);
	
	List<JC> selJobByJob(String job_name);
	
}
