package com.xqxt.mapper;

import com.xqxt.entity.Student;

public interface StudentMapper {

	void insStudent(Student student);
	
	Student selStudent(String s_email);
	
}
