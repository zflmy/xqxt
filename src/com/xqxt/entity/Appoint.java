package com.xqxt.entity;

/**
 * 
 * 公司预约校招类
 * @author dell
 *
 */
public class Appoint {

	private String c_email;       //企业预约校招编号
	private String company_name;  //公司名称
	private String appoint_time;  //预约时间
	
	public String getC_email() {
		return c_email;
	}
	public void setC_email(String c_email) {
		this.c_email = c_email;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getAppoint_time() {
		return appoint_time;
	}
	public void setAppoint_time(String appoint_time) {
		this.appoint_time = appoint_time;
	}
	
	
	
}
