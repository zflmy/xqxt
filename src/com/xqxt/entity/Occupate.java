package com.xqxt.entity;

/**
 * 职位类类
 * @author dell
 *
 */
public class Occupate {

	private String occupate_no;    //职位类编号
	private String occupate_name;  //职位类名称
	
	public Occupate() {}
	
	public Occupate(String occupate_no, String occupate_name) {
		this.occupate_no = occupate_no;
		this.occupate_name = occupate_name;
	}

	public String getOccupate_no() {
		return occupate_no;
	}

	public void setOccupate_no(String occupate_no) {
		this.occupate_no = occupate_no;
	}

	public String getOccupate_name() {
		return occupate_name;
	}

	public void setOccupate_name(String occupate_name) {
		this.occupate_name = occupate_name;
	}
	
}
