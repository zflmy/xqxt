package com.xqxt.entity;

/**
 * 简历与公司关系类
 * @author dell
 *
 */
public class RC {

	private String c_email;  //公司编号
	private String s_email;   //简历编号
	private String job_no;      //职位编号
	private String state_msg;   //投递状态信息
	
	public RC() {}

	public RC(String c_email, String s_email, String job_no, String state_msg) {
		this.c_email = c_email;
		this.s_email = s_email;
		this.job_no = job_no;
		this.state_msg = state_msg;
	}

	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public String getS_email() {
		return s_email;
	}

	public void setS_email(String s_email) {
		this.s_email = s_email;
	}

	public String getJob_no() {
		return job_no;
	}

	public void setJob_no(String job_no) {
		this.job_no = job_no;
	}

	public String getState_msg() {
		return state_msg;
	}

	public void setState_msg(String state_msg) {
		this.state_msg = state_msg;
	}

	
	
}
