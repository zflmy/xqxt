package com.xqxt.entity;

/**
 * 
 * 统计类
 * @author dell
 *
 */
public class Count {

	private String academy_name;   //学院名称
	private String major_name;     //专业名称
	private String job_state;      //就业状态
	private int num;               //对应数量
	
	public Count() {}
	
	public Count(String job_state, int num) {
		this.job_state = job_state;
		this.num = num;
	}
	
	public Count(String major_name, String job_state, int num) {
		this.major_name = major_name;
		this.job_state = job_state;
		this.num = num;
	}
	
	public Count(String academy_name, String major_name, String job_state, int num) {
		this.academy_name = academy_name;
		this.major_name = major_name;
		this.job_state = job_state;
		this.num = num;
	}

	public String getAcademy_name() {
		return academy_name;
	}

	public void setAcademy_name(String academy_name) {
		this.academy_name = academy_name;
	}

	public String getMajor_name() {
		return major_name;
	}

	public void setMajor_name(String major_name) {
		this.major_name = major_name;
	}

	public String getJob_state() {
		return job_state;
	}

	public void setJob_state(String job_state) {
		this.job_state = job_state;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "Count [academy_name=" + academy_name + ", major_name=" + major_name + ", job_state=" + job_state
				+ ", num=" + num + "]";
	}
	
}
