package com.xqxt.entity;


/**
 * 
 * 构造类
 * @author dell
 *
 */
public class JOC {

	private String c_email;    //公司编号
	private String occupate_no;   //职位类编号
	private String job_no;        //职位编号
	private String s_email;     //学生邮箱
	private String s_name;        //姓名
	private String state_msg;     //简历投递信息
	private String job_name;      //职位名称
	private String need_num;      //需求人数
	private String salary;        //薪资待遇
	private String job_required;  //职位需求
	private String company_name;  //公司名称
	private double grade;         //评分
	private String city;          //位置城市
	public String getC_email() {
		return c_email;
	}
	public void setC_email(String c_email) {
		this.c_email = c_email;
	}
	public String getOccupate_no() {
		return occupate_no;
	}
	public void setOccupate_no(String occupate_no) {
		this.occupate_no = occupate_no;
	}
	public String getJob_no() {
		return job_no;
	}
	public void setJob_no(String job_no) {
		this.job_no = job_no;
	}
	public String getS_email() {
		return s_email;
	}
	public void setS_email(String s_email) {
		this.s_email = s_email;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public String getState_msg() {
		return state_msg;
	}
	public void setState_msg(String state_msg) {
		this.state_msg = state_msg;
	}
	public String getJob_name() {
		return job_name;
	}
	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}
	public String getNeed_num() {
		return need_num;
	}
	public void setNeed_num(String need_num) {
		this.need_num = need_num;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getJob_required() {
		return job_required;
	}
	public void setJob_required(String job_required) {
		this.job_required = job_required;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public double getGrade() {
		return grade;
	}
	public void setGrade(double grade) {
		this.grade = grade;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
