package com.xqxt.entity;

/**
 * 专业类
 * @author dell
 *
 */
public class Major {

	private String major_no;   //专业编号
	private String major_name; //专业名称
	private String academy_no; //学院编号
	
	public Major() {}
	
	public Major(String major_no, String major_name, String academy_no) {
		this.major_no = major_no;
		this.major_name = major_name;
		this.academy_no = academy_no;
	}

	public String getMajor_no() {
		return major_no;
	}

	public void setMajor_no(String major_no) {
		this.major_no = major_no;
	}

	public String getMajor_name() {
		return major_name;
	}

	public void setMajor_name(String major_name) {
		this.major_name = major_name;
	}

	public String getAcademy_no() {
		return academy_no;
	}

	public void setAcademy_no(String academy_no) {
		this.academy_no = academy_no;
	}
	
}
