package com.xqxt.entity;

/**
 * 
 * 公司类
 * @author dell
 *
 */
public class Company {

	private String c_email;     //公司编号
	private String company_name;   //公司名称
	private String company_intro;  //公司简介
	private String city;           //公司位置
	private int have_num;          //拥有人数
	private double grade;          //评分
	private String phone;          //电话
	public Company() {}

	

	public Company(String c_email, String company_name, String company_intro, String city, int have_num, double grade,
			String phone) {
		this.c_email = c_email;
		this.company_name = company_name;
		this.company_intro = company_intro;
		this.city = city;
		this.have_num = have_num;
		this.grade = grade;
		this.phone = phone;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getCompany_intro() {
		return company_intro;
	}

	public void setCompany_intro(String company_intro) {
		this.company_intro = company_intro;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getHave_num() {
		return have_num;
	}

	public void setHave_num(int have_num) {
		this.have_num = have_num;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}
	
	

	
	
}

