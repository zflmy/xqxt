package com.xqxt.entity;

public class Admin {

	private String academy_no;
	private String academy_name;
	private String major_no;
	private String major_name;
	private String class_name;
	private String s_no;
	private String s_email;
	
	private String s_name;
	private String unit_no;
	private String unit_name;
	private String unit_type;
	private String job_state;
	private String protocoal_img;
	private String pass_state;
	public String getAcademy_no() {
		return academy_no;
	}
	public void setAcademy_no(String academy_no) {
		this.academy_no = academy_no;
	}
	public String getAcademy_name() {
		return academy_name;
	}
	public void setAcademy_name(String academy_name) {
		this.academy_name = academy_name;
	}
	public String getMajor_no() {
		return major_no;
	}
	public void setMajor_no(String major_no) {
		this.major_no = major_no;
	}
	public String getMajor_name() {
		return major_name;
	}
	public void setMajor_name(String major_name) {
		this.major_name = major_name;
	}
	public String getClass_name() {
		return class_name;
	}
	public void setClass_name(String class_name) {
		this.class_name = class_name;
	}
	public String getS_no() {
		return s_no;
	}
	public void setS_no(String s_no) {
		this.s_no = s_no;
	}
	public String getS_email() {
		return s_email;
	}
	public void setS_email(String s_email) {
		this.s_email = s_email;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public String getUnit_no() {
		return unit_no;
	}
	public void setUnit_no(String unit_no) {
		this.unit_no = unit_no;
	}
	public String getUnit_name() {
		return unit_name;
	}
	public void setUnit_name(String unit_name) {
		this.unit_name = unit_name;
	}
	public String getUnit_type() {
		return unit_type;
	}
	public void setUnit_type(String unit_type) {
		this.unit_type = unit_type;
	}
	public String getJob_state() {
		return job_state;
	}
	public void setJob_state(String job_state) {
		this.job_state = job_state;
	}
	public String getProtocoal_img() {
		return protocoal_img;
	}
	public void setProtocoal_img(String protocoal_img) {
		this.protocoal_img = protocoal_img;
	}
	public String getPass_state() {
		return pass_state;
	}
	public void setPass_state(String pass_state) {
		this.pass_state = pass_state;
	}
	
	
	
}
