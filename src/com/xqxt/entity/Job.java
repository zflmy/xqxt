package com.xqxt.entity;

/**
 * 职位类
 * @author dell
 *
 */
public class Job {

	private String job_no;          //职位编号
	private String job_name;        //职位名称
	private String job_required;    //职位需求
	private String occupate_no;     //职位类编号
	
	public Job() {}
	
	public Job(String job_no, String job_name, String job_required, String occupate_no) {
		this.job_no = job_no;
		this.job_name = job_name;
		this.occupate_no = occupate_no;
	}

	public String getJob_no() {
		return job_no;
	}

	public void setJob_no(String job_no) {
		this.job_no = job_no;
	}

	public String getJob_name() {
		return job_name;
	}

	public void setJob_name(String job_name) {
		this.job_name = job_name;
	}

	public String getJob_required() {
		return job_required;
	}

	public void setJob_required(String job_required) {
		this.job_required = job_required;
	}

	public String getOccupate_no() {
		return occupate_no;
	}

	public void setOccupate_no(String occupate_no) {
		this.occupate_no = occupate_no;
	}
	
}
