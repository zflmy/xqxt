package com.xqxt.entity;

/**
 * 公告类
 * @author dell
 *
 */
public class Notice {

	private int notice_no;   //公告编号
	private String time;     //发布时间
	private String content;  //公告内容
	private String sendto;	//发送给谁
	
	public Notice() {}
	
	

	public Notice(int notice_no, String time, String content, String sendto) {
		this.notice_no = notice_no;
		this.time = time;
		this.content = content;
		this.sendto = sendto;
	}



	public int getNotice_no() {
		return notice_no;
	}



	public void setNotice_no(int notice_no) {
		this.notice_no = notice_no;
	}



	public String getTime() {
		return time;
	}



	public void setTime(String time) {
		this.time = time;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	public String getSendto() {
		return sendto;
	}



	public void setSendto(String sendto) {
		this.sendto = sendto;
	}



	
	
}
