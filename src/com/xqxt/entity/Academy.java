package com.xqxt.entity;

/**
 * 学院类
 * @author dell
 *
 */
public class Academy {

	private String academy_no;   //学院编号
	private String academy_name; //学院名称
	
	//构造方法
	public Academy() {}
	
	//构造方法
	public Academy(String academy_no, String academy_name) {
		this.academy_no = academy_no;
		this.academy_name = academy_name;
	}

	public String getAcademy_no() {
		return academy_no;
	}

	public void setAcademy_no(String academy_no) {
		this.academy_no = academy_no;
	}

	public String getAcademy_name() {
		return academy_name;
	}

	public void setAcademy_name(String academy_name) {
		this.academy_name = academy_name;
	}
	
}
