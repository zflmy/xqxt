package com.xqxt.entity;

/**
 * 学生类
 * @author dell
 *
 */
public class Student {

	private String s_no;             //学号
	private String s_name;           //名字
	private String class_name;         //专业编号
	private String s_email;        //简历编号
	private String job_state;        //就业状态
	private String unit_no;
	private String protocoal_img;
	private String pass_state;
	
	public String getS_no() {
		return s_no;
	}

	public void setS_no(String s_no) {
		this.s_no = s_no;
	}

	public String getS_name() {
		return s_name;
	}

	public void setS_name(String s_name) {
		this.s_name = s_name;
	}

	public String getClass_name() {
		return class_name;
	}

	public void setClass_name(String class_name) {
		this.class_name = class_name;
	}

	public String getS_email() {
		return s_email;
	}

	public void setS_email(String s_email) {
		this.s_email = s_email;
	}

	public String getJob_state() {
		return job_state;
	}

	public void setJob_state(String job_state) {
		this.job_state = job_state;
	}

	public String getUnit_no() {
		return unit_no;
	}

	public void setUnit_no(String unit_no) {
		this.unit_no = unit_no;
	}

	public String getProtocoal_img() {
		return protocoal_img;
	}

	public void setProtocoal_img(String protocoal_img) {
		this.protocoal_img = protocoal_img;
	}

	public String getPass_state() {
		return pass_state;
	}

	public void setPass_state(String pass_state) {
		this.pass_state = pass_state;
	}

	@Override
	public String toString() {
		return "Student [s_no=" + s_no + ", s_name=" + s_name + ", class_name=" + class_name + ", s_email="
				+ s_email + ", job_state=" + job_state + ", unit_no=" + unit_no + ", protocoal_img=" + protocoal_img
				+ ", pass_state=" + pass_state + "]";
	}
	
}
