package com.xqxt.entity;

/**
 * 公司职位关系类
 * @author dell
 *
 */
public class JC {

	private String c_email;    //公司编号
	private String job_no;        //职位编号
	private String occupate_no;   //职业类编号
	private int need_num;         //需求人数
	private String salary;        //薪资
	
	public JC() {}
	
	public JC(String c_email, String job_no, String occupate_no, int need_num, String salary) {
		this.c_email = c_email;
		this.job_no = job_no;
		this.occupate_no = occupate_no;
		this.need_num = need_num;
		this.salary = salary;
	}

	public String getC_email() {
		return c_email;
	}

	public void setC_email(String c_email) {
		this.c_email = c_email;
	}

	public String getJob_no() {
		return job_no;
	}

	public void setJob_no(String job_no) {
		this.job_no = job_no;
	}

	public String getOccupate_no() {
		return occupate_no;
	}

	public void setOccupate_no(String occupate_no) {
		this.occupate_no = occupate_no;
	}

	public int getNeed_num() {
		return need_num;
	}

	public void setNeed_num(int need_num) {
		this.need_num = need_num;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	
	
}
