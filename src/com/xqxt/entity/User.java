package com.xqxt.entity;

/**
 * 
 * 用户类
 * @author dell
 *
 */
public class User {

	private String user_email;  //邮箱
	private String user_pass;  //密码
	private String user_type;  //用户类型
	private String auth_code;  //验证码
	
	public User() {}
	

	public User(String user_email, String user_pass, String user_type) {
		this.user_email = user_email;
		this.user_pass = user_pass;
		this.user_type = user_type;
	}
	
	public User(String user_email, String auth_code) {
		this.user_email = user_email;
		this.auth_code = auth_code;
	}




	public String getUser_email() {
		return user_email;
	}


	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}


	public String getUser_pass() {
		return user_pass;
	}


	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}


	public String getUser_type() {
		return user_type;
	}


	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}


	public String getAuth_code() {
		return auth_code;
	}


	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}


	
	
}
