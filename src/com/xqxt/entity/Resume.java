package com.xqxt.entity;

/**
 * 简历类
 * @author dell
 *
 */
public class Resume {

	private String s_email;     //简历编号
	private String name;          //名字
	private String sex;           //性别
	private int age;              //年龄
	private String record_edu;    //学历
	private String phone;         //电话
	private String head;          //头像
	private String school_name;   //学校名称
	private String major;         //专业
	private String start_stop_time; //起止时间
	private String score_rank;    //成绩排名
	private String skilled;       //专业技能
	private String hoppy;         //爱好
	private String prize;         //奖项
	private String self_assessment;  //自我评价
	
	public Resume() {}

	public Resume(String s_email, String name, String sex, int age, String record_edu,  String phone, String head,
			String school_name, String major, String start_stop_time, String score_rank, String skilled,
			String hoppy, String prize, String self_assessment) {
		this.s_email = s_email;
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.record_edu = record_edu;
		this.phone = phone;
		this.head = head;
		this.school_name = school_name;
		this.major = major;
		this.start_stop_time = start_stop_time;
		this.score_rank = score_rank;
		this.skilled = skilled;
		this.hoppy = hoppy;
		this.prize = prize;
		this.self_assessment = self_assessment;
	}

	

	public String getS_email() {
		return s_email;
	}

	public void setS_email(String s_email) {
		this.s_email = s_email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRecord_edu() {
		return record_edu;
	}

	public void setRecord_edu(String record_edu) {
		this.record_edu = record_edu;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getSchool_name() {
		return school_name;
	}

	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStart_stop_time() {
		return start_stop_time;
	}

	public void setStart_stop_time(String start_stop_time) {
		this.start_stop_time = start_stop_time;
	}

	public String getScore_rank() {
		return score_rank;
	}

	public void setScore_rank(String score_rank) {
		this.score_rank = score_rank;
	}

	public String getSkilled() {
		return skilled;
	}

	public void setSkilled(String skilled) {
		this.skilled = skilled;
	}

	public String getHoppy() {
		return hoppy;
	}

	public void setHoppy(String hoppy) {
		this.hoppy = hoppy;
	}

	public String getPrize() {
		return prize;
	}

	public void setPrize(String prize) {
		this.prize = prize;
	}

	public String getSelf_assessment() {
		return self_assessment;
	}

	public void setSelf_assessment(String self_assessment) {
		this.self_assessment = self_assessment;
	}

	@Override
	public String toString() {
		return "Resume [s_email=" + s_email + ", name=" + name + ", sex=" + sex + ", age=" + age + ", record_edu="
				+ record_edu + ",  phone=" + phone + ", head=" + head + ", school_name="
				+ school_name + ", major=" + major + ", start_stop_time=" + start_stop_time + ", score_rank="
				+ score_rank + ", skilled=" + skilled + ", hoppy=" + hoppy + ", prize=" + prize + ", self_assessment="
				+ self_assessment + "]";
	}
	
}
