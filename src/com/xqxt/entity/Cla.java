package com.xqxt.entity;

public class Cla {

	private String class_name;
	private String major_no;
	
	public String getClass_name() {
		return class_name;
	}
	public void setClass_name(String class_name) {
		this.class_name = class_name;
	}
	public String getMajor_no() {
		return major_no;
	}
	public void setMajor_no(String major_no) {
		this.major_no = major_no;
	}
	
}
