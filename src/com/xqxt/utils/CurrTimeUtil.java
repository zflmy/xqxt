package com.xqxt.utils;

import java.text.SimpleDateFormat;

public class CurrTimeUtil {
	
	//获取本地时间
	public static String getTime() {
		long time = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curr_time = sdf.format(time);
		
		return curr_time;
	}

}
