package com.xqxt.utils;

public class ResumeUtil {

	//改变简历投递状态
	public static String getState(int n) {
		
		if(n == 1) {
			return "简历已投递到HR邮箱，正在等待HR浏览~，请随时关注哦......";
		}
		if(n == 2) {
			return "我正在浏览你的简历，可能近期会预约你面试哦~，请保证电话畅通......";
		}
		if(n == 3) {
			return "你的简历符合我们的要求，我们诚邀您明天下午来面试哦~，如有时间上的安排，请随时联系我们哦......";
		}
		else {
			return "您已通过面试~，请您在一到两个工作日来入职......";
		}
		
	}
	
}
