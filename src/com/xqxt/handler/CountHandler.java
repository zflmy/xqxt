package com.xqxt.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.Admin;
import com.xqxt.entity.Result;
import com.xqxt.service.CountService;

@Controller
public class CountHandler {

	@Autowired
	private CountService countService;
	
	@ResponseBody
	@RequestMapping("selAllNum")
	public String selAllNum(Admin admin) {
		int count = countService.selAllNum(admin);
		
		return count+"";
	}
	
	@ResponseBody
	@RequestMapping("selState")
	public String selState(Admin admin) {
		List<Result> ls = countService.selState(admin);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selType")
	public String selType(Admin admin) {
		List<Result> ls = countService.selType(admin);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selByType")
	public String selByType(Admin admin) {
		int count = countService.selByType(admin);
		
		return count+"";
	}
	
}
