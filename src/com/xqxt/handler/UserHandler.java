package com.xqxt.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xqxt.entity.User;
import com.xqxt.service.StudentService;
import com.xqxt.service.UserService;
import com.xqxt.utils.JSONUtil;
import com.xqxt.utils.JavaMailUtils;

@Controller
public class UserHandler {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StudentService studentService;
	
	private static List<User> registerAuthCodeUsers=new ArrayList<>();
	private static List<User> findPassSendEmailUsers=new ArrayList<>();
	@RequestMapping("findPassSendEmail")
	public void findPassSendEmail(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String email = req.getParameter("email");
        
		if(!isHaveEmail(email)) {
			HashMap<String, Object> map = new HashMap<String,Object>();
            map.put("result","unexit");
            JSONUtil.printByJSON(resp,map);
		}else {
			 Random rm = new Random();
			 String auth_code = "";
			 for(int i=0;i<5;i++) {
				int num = rm.nextInt(26)+65;
	       	  	auth_code += (char)num;
	        }
	        
	        int res=JavaMailUtils.sendEmail(new String[]{
	    			email //这里就是一系列的收件人的邮箱了
	    	}, "学企新途邮箱验证", "您正在执行更改密码操作，若不是您在操作，请不要透露验证码。您的验证码是 <br />"+auth_code);
	       if(res==1) {//如果发送成功
	    	   Iterator<User> iterator1 = findPassSendEmailUsers.iterator();
      			while (iterator1.hasNext()){
      				User next1 = iterator1.next();
      				if(next1.getUser_email().equals(email)) {
      					iterator1.remove();
      				}
              }
	    	   User u =new User(email,auth_code);
	    	   findPassSendEmailUsers.add(u);
	       		HashMap<String, Object> map = new HashMap<String,Object>();
	            map.put("result",true);
	            map.put("auth_code",auth_code);
	            JSONUtil.printByJSON(resp,map);
	       }else {
	       	HashMap<String, Object> map = new HashMap<String,Object>();
	           map.put("result",false);
	           JSONUtil.printByJSON(resp,map);
			}
		}

	}
	@RequestMapping("findPass")
	public String findPass(HttpServletRequest req,String password ,String email,String auth_code) {
	
		if(!isHaveEmail(email)) {
			req.setAttribute("msg","该邮箱未注册，请注册");
    	    return "login";
		}
		
		Iterator<User> iterator = findPassSendEmailUsers.iterator();
		while (iterator.hasNext()){
			User next = iterator.next();
            if(next.getUser_email().equals(email)&&next.getAuth_code().equals(auth_code)){
            	iterator.remove();
            	User user = new User();
            	user.setUser_email(email);
            	user.setUser_pass(password);
            	userService.updUser(user);
            	req.setAttribute("msg","找回密码成功，请登录");
        	    return "login";
            }
        }
	
		req.setAttribute("err", "验证码错误,请重新发送验证码");
	    return "findPass";
	}
	
	
	
	
	@RequestMapping("registerAuthCode")
	public void registerAuthCode(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String email = req.getParameter("email");
        
		if(isHaveEmail(email)) {
			HashMap<String, Object> map = new HashMap<String,Object>();
            map.put("result","exit");
            JSONUtil.printByJSON(resp,map);;
		}else {
			 Random rm = new Random();
	        String auth_code = "";
	        for(int i=0;i<5;i++) {
	       	  int num = rm.nextInt(26)+65;
	       	  auth_code += (char)num;
	        }
	        
	        int res=JavaMailUtils.sendEmail(new String[]{
	    			email //这里就是一系列的收件人的邮箱了
	    	}, "学企新途邮箱验证", "欢迎您注册学企新途，您的验证码是 <br />"+auth_code);
	       if(res==1) {//如果发送成功
	    	   //查看列表中是否已经存在该邮箱，若存在则删除
	    	   Iterator<User> iterator1 = registerAuthCodeUsers.iterator();
       			while (iterator1.hasNext()){
       				User next1 = iterator1.next();
       				if(next1.getUser_email().equals(email)) {
       					iterator1.remove();
       				}
               }
       			
	    	   User u =new User(email,auth_code);
	    	   registerAuthCodeUsers.add(u);
	    	   
       			
	       		HashMap<String, Object> map = new HashMap<String,Object>();
	            map.put("result",true);
	            map.put("auth_code",auth_code);
	            JSONUtil.printByJSON(resp,map);
	       }else {
	       	HashMap<String, Object> map = new HashMap<String,Object>();
	           map.put("result",false);
	           JSONUtil.printByJSON(resp,map);
			}
		}
		
       
		
	}
	@RequestMapping("admRegUser")
	public String admRegUser(HttpServletRequest req,String user_email ,String user_type) {
		if(isHaveEmail(user_email)) {
			req.setAttribute("msg","该邮箱已经被注册，请更换注册");
    	    return "admAddUser";
		}
		userService.insUser(new User(user_email,"000000",user_type));
		req.setAttribute("msg", "注册成功，默认密码为：000000");
		return "admAddUser";
	}
	@RequestMapping("register")
	public String insUser(HttpServletRequest req,String password ,String email,String auth_code, String user_type) {
	
		if(isHaveEmail(email)) {
			req.setAttribute("msg","该邮箱已经被注册，请登陆");
    	    return "login";
		}
		
		Iterator<User> iterator = registerAuthCodeUsers.iterator();
		while (iterator.hasNext()){
			User next = iterator.next();
            if(next.getUser_email().equals(email)&&next.getAuth_code().equals(auth_code)){
            	iterator.remove();
            	
            	userService.insUser(new User(email, password, user_type));
            	if(user_type.equals("学生")) {
            		userService.insResumeEmail(email);
            	}
            	req.setAttribute("msg","注册成功，请登录");
        	    return "login";
            }
        }
		req.setAttribute("err", "验证码错误,请重新发送验证码");
	    return "studentZhuce";
	}

	private boolean isHaveEmail(String email) {
		List<User> allUsers = userService.selAll();
		Iterator<User> allusersIterator = allUsers.iterator();
		boolean isHaveEmail=false;
		while (allusersIterator.hasNext()){
			User next = allusersIterator.next();
			if(next.getUser_email().equals(email)) {
				isHaveEmail = true;
				
			}
		}
		return isHaveEmail;
	}
	@RequestMapping("delUser")
	public String delUser(String user_name) {
		userService.delUser(user_name);
		
		return "success";
	}
	
	@RequestMapping("updPass")
	public String updUser(HttpServletRequest req,HttpServletResponse resp,String stu_email, String user_pass, String new_pass) {
		List<User> allUsers = userService.selAll();
		String trueUser_pass = null;
		System.out.println("修改密码 传来的密码"+user_pass);
		Iterator<User> allUsersIterable = allUsers.iterator();
		while (allUsersIterable.hasNext()){
			User next = allUsersIterable.next();
			if(next.getUser_email().equals(stu_email)&&next.getUser_pass().equals(user_pass)) {
				trueUser_pass = next.getUser_pass();
			}
		}
		System.out.println("到了修改密码这++++++++++");
		System.out.println("修改密码+++++++"+stu_email);
		System.out.println("修改密码 真正的密码"+trueUser_pass);
		System.out.println();
		if(trueUser_pass!=null) {
			if(trueUser_pass.equals(new_pass)) {
				req.setAttribute("updPassMsg", "修改的密码和原密码相同");
				return "updPass";
			}else {
				userService.updUser(new User(stu_email, new_pass,""));	
				req.setAttribute("updPassMsg", "修改成功");
				return "updPass";
			}
		}else {
			req.setAttribute("updPassMsg", "原密码错误");
			return "updPass";
		}
		
	}
	
	@RequestMapping("login")
	public String selUser(HttpServletRequest req, User user) {

		User u = userService.selUser(user);

		if(u == null) {
			req.setAttribute("msg", "账号或密码错误，请重试！");
			return "login";
		}
		req.setAttribute("user_email", u.getUser_email());
		if(user.getUser_type().equals("学生")) {
//			if(studentService.selStudent(user.getUser_email()) == null || studentService.selStudent(user.getUser_email()).getProtocoal_img().equals("")) {
//				return "stuMsg";
//			} else {
				
				return "stuMain";
//			}
		} else if(user.getUser_type().equals("企业")) {
//			return "redirect:/selRC";
			return "companyMain";
		}else if(user.getUser_type().equals("管理员")) {
			return "admin";
		}
		return null;
	}
	
}
