package com.xqxt.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xqxt.entity.Student;
import com.xqxt.service.StudentService;

@Controller
public class StudentHandler {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping("insStudent")
	public String insStudent(Student student) {
		studentService.insStudent(student);
		
		return "redirect:/selCompany";
	}
	
}
