package com.xqxt.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xqxt.entity.Company;
import com.xqxt.entity.JOC;
import com.xqxt.entity.Notice;
import com.xqxt.entity.User;
import com.xqxt.service.NoticeService;
import com.xqxt.utils.CurrTimeUtil;
import com.xqxt.utils.TextUtil;

@Controller
public class NoticeHandler {

	@Autowired
	private NoticeService noticeService;
	@RequestMapping("lookNotice")
	public String lookNotice(HttpServletRequest req ,String sendto) {
		System.out.println(sendto);
		if (sendto.equals("学生")||sendto.equals("企业")||sendto.equals("全部")) {
			List<Notice> noticeLs = noticeService.selNotice(sendto);
			req.setAttribute("noticeLs", noticeLs);
			
			return "lookNotice";
		}else {
			req.setAttribute("noticeLs", "请求错误，请稍后重试");
			
			return "lookNotice";
		}
		
	}
	
	
	@RequestMapping("insNotice")
	public String insNotice(HttpServletRequest req,String content,String sendto) {
		System.out.println(content);
		System.out.println(sendto);
		if (content==null||content=="") {
			req.setAttribute("msg", "公告内容不能为空");
			return "sendNotice";
		}
		if (sendto.equals("学生")||sendto.equals("企业")||sendto.equals("全部")) {
			noticeService.insNotice(new Notice(0, CurrTimeUtil.getTime(), content,sendto));
			req.setAttribute("msg",	"发布成功");
			return "sendNotice";
			
			
		}else {
			req.setAttribute("msg", "发送对象错误");
			return "sendNotice";
		}
		
	}
	
}
