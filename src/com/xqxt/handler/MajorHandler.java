package com.xqxt.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.Major;
import com.xqxt.service.MajorService;

@Controller
public class MajorHandler {

	@Autowired
	private MajorService majorService;
	
	@RequestMapping("selMajor")
	@ResponseBody
	public String selMajorByAcademy(String academy_no) {
		List<Major> ls = majorService.selMajorByAcademy(academy_no);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	@RequestMapping("selMajors")
	@ResponseBody
	public String selAllMajor(String academy_no) {
		List<Major> ls = majorService.selALLMajor();
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
