package com.xqxt.handler;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xqxt.entity.Appoint;
import com.xqxt.entity.Company;
import com.xqxt.entity.User;
import com.xqxt.service.AppointService;
import com.xqxt.service.CompanyService;
import com.xqxt.utils.JSONUtil;

@Controller
public class AppointHandler {

	@Autowired
	private AppointService appointService;
	
	@Autowired
	private CompanyService companyService; 
	@RequestMapping("com_predict")
	public String com_predict(HttpServletRequest req,String com_email) {
		 Appoint selOneAppoint = appointService.selOneAppoint(com_email);
		 if(selOneAppoint!=null) {
			 req.setAttribute("appointTime", selOneAppoint.getAppoint_time());
		 }
		
		req.setAttribute("com_email", com_email);
		return "com_predict";
	}
	@RequestMapping("updAppoint")
	public void updAppoint(HttpServletRequest req,HttpServletResponse resp) {
		String com_email = req.getParameter("com_email");
		String appoint_time = req.getParameter("appoint_time");
		
		Appoint appoint = new Appoint();
		
		Company company = companyService.selCompanyByNo(com_email);
		appoint.setCompany_name(company.getCompany_name());
		appoint.setC_email(com_email);
		appoint.setAppoint_time(appoint_time);
		appointService.updAppoint(appoint);
		
		HashMap<String, Object> map = new HashMap<String,Object>();
        map.put("result",true);
        JSONUtil.printByJSON(resp,map);
	}	
	
	@RequestMapping("insAppoint")
	public void insAppoint(HttpServletRequest req, HttpServletResponse resp) {
		String com_email = req.getParameter("com_email");
		String appoint_time = req.getParameter("appoint_time");
		
		Appoint appoint = new Appoint();
		
		Company company = companyService.selCompanyByNo(com_email);
		appoint.setCompany_name(company.getCompany_name());
		appoint.setC_email(com_email);
		appoint.setAppoint_time(appoint_time);
		appointService.insAppoint(appoint);
		
		HashMap<String, Object> map = new HashMap<String,Object>();
        map.put("result",true);
        JSONUtil.printByJSON(resp,map);
		
	}	
}
