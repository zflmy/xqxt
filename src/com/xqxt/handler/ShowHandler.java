package com.xqxt.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.Count;
import com.xqxt.service.ShowService;

@Controller
public class ShowHandler {

	@Autowired
	private ShowService showService;
	
	@ResponseBody
	@RequestMapping("selWhole")
	public String selWhole() {
		List<Count> ls = showService.selWhole();
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selByAcademy")
	public String selByAcademy(String academy_name) {
		List<Count> ls = showService.selByAcademy(academy_name);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selByAcademys")
	public String selByAcademys() {
		List<Count> ls = showService.selAcademys();
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
