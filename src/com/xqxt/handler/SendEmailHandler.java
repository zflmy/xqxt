package com.xqxt.handler;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@SuppressWarnings("serial")

@WebServlet("/SendEmail")
public class SendEmailHandler extends HttpServlet{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // TODO Auto-generated method stub
        resp.getWriter().append("Served at: ").append(req.getContextPath());
        System.out.println(req.getContextPath());
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");

        String md = req.getParameter("md");
        if(md!=null && !md.equals("")) {
            try {
                // 使用反射，根据md找到对应的方法，并调用该方法
                Method method = this.getClass().getMethod(md, HttpServletRequest.class, HttpServletResponse.class);
                // 调用该方法
                method.invoke(this, req, resp);

            } catch(Exception e) {
                e.printStackTrace();
            }
        } else {
                req.setAttribute("msg", "   请求错误，请重试！");
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }
   
}
