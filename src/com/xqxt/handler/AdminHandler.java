package com.xqxt.handler;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.Academy;
import com.xqxt.entity.Admin;
import com.xqxt.entity.Appoint;
import com.xqxt.entity.Cla;
import com.xqxt.entity.Company;
import com.xqxt.entity.Major;
import com.xqxt.entity.Student;
import com.xqxt.entity.Unit;
import com.xqxt.entity.User;
import com.xqxt.service.AcademyService;
import com.xqxt.service.AdminService;
import com.xqxt.service.AppointService;
import com.xqxt.service.CompanyService;
import com.xqxt.service.MajorService;
import com.xqxt.utils.JSONUtil;

@Controller
public class AdminHandler {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private AppointService appointService;
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private MajorService majorService;
	
	private boolean isHavaAcademy(String academy_no) {
		List<Academy> selAllAcademy = adminService.selAllAcademy();
		Iterator<Academy> iterator = selAllAcademy.iterator();
		while(iterator.hasNext()) {
			Academy next = iterator.next();
			if(next.getAcademy_no().equals(academy_no)) {
				return true;
			}
		}
		return false;
	}
	private boolean isHavaMajor(String major_no) {
		List<Major> selALLMajor = majorService.selALLMajor();
		Iterator<Major> iterator = selALLMajor.iterator();
		while(iterator.hasNext()) {
			Major next = iterator.next();
			if(next.getMajor_no().equals(major_no)) {
				return true;
			}
		}
		return false;
	}
	@ResponseBody
	@RequestMapping("selAllAcademy")
	public String selAllAcademy() {
		List<Academy> ls = adminService.selAllAcademy();
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	@RequestMapping("selAllAcademyToPage")
	public String selAllAcademyToPage(HttpServletRequest req, Admin admin) {
		List<Academy> ls = adminService.selAllAcademy();
		req.setAttribute("allAcademy", ls);
		return "admGLAcademy";
	}
	@RequestMapping("selAllMajorToPage")
	public String selAllMajorToPage(HttpServletRequest req,Admin admin) {
		List<Admin> ls =adminService.selAllMajor(admin);
		req.setAttribute("allMajor",ls);
		
		return "admGLMajor";
	}
	@RequestMapping("admSelAllCom")
	public String admSelAllCom(HttpServletRequest req,Admin admin) {
		List<Company> ls= companyService.selCompany();
		req.setAttribute("allcom",ls);
		return "admGLCompany";
	}
	@RequestMapping("selAllProtoToPage")
	public String selAllProtoToPage(HttpServletRequest req,Admin admin) {
		List<Admin> ls = adminService.selAllProto(admin);
		req.setAttribute("allPro", ls);
		return "admGLPro";
	}
	@RequestMapping("selAllClassToPage")
	public String selAllClassToPage(HttpServletRequest req,Admin admin) {
		List<Admin> ls = adminService.selAllClass(admin);
		req.setAttribute("allClass", ls);
		return "admGLClass";
	}
	@RequestMapping("admLookComAppoint")
	public String admLookComAppoint(HttpServletRequest req) {
		List<Appoint> selAppoint = appointService.selAppoint();
		req.setAttribute("allAppoints", selAppoint);
		
		return "admLookComAppoint";
	}
	@ResponseBody
	@RequestMapping("insAcademy")
	public String insAcademy(Academy academy) {
		adminService.insAcademy(academy);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(academy);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping("updAcademy")
	public void updAcademy(HttpServletResponse resp,String oldAcademy_no,String academy_no,String academy_name ) {
		if(oldAcademy_no.equals(academy_no)) {
			adminService.updAcademy(oldAcademy_no,academy_no,academy_name);
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","ok");
	        JSONUtil.printByJSON(resp,map);
		}else {
			if(!isHavaAcademy(academy_no)) {
				adminService.updAcademy(oldAcademy_no,academy_no,academy_name);
				HashMap<String, Object> map = new HashMap<String,Object>();
		        map.put("result","ok");
		        JSONUtil.printByJSON(resp,map);
			}else {
				HashMap<String, Object> map = new HashMap<String,Object>();
		        map.put("result","unexit");
		        JSONUtil.printByJSON(resp,map);
			}
		}
	}
	

	@RequestMapping("delAcademy")
	public void updAcademy(HttpServletResponse resp,String academy_no) {
		adminService.delAcademy(academy_no);
		HashMap<String, Object> map = new HashMap<String,Object>();
        map.put("result","ok");
        JSONUtil.printByJSON(resp,map);
	}
	
	@ResponseBody
	@RequestMapping("selAllMajor")
	public String selAllMajor(Admin admin ,HttpServletRequest req) {
		List<Admin> ls = adminService.selAllMajor(admin);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("insMajor")
	public String insMajor(Major major) {
		adminService.insMajor(major);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(major);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping("updMajor")
	public void updMajor(HttpServletResponse resp,String oldMajor_no,String academy_no,String major_no,String major_name ) {
		if(oldMajor_no.equals(major_no)) {
			adminService.updMajor(oldMajor_no,academy_no,major_no,major_name);
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","ok");
	        JSONUtil.printByJSON(resp,map);
		}else {
			if(!isHavaMajor(major_no)) {
				adminService.updMajor(oldMajor_no,academy_no,major_no,major_name);
				HashMap<String, Object> map = new HashMap<String,Object>();
		        map.put("result","ok");
		        JSONUtil.printByJSON(resp,map);
			}else {
				HashMap<String, Object> map = new HashMap<String,Object>();
		        map.put("result","unexit");
		        JSONUtil.printByJSON(resp,map);
			}
		}
	}
	
	@RequestMapping("delMajor")
	public void delMajor(HttpServletResponse resp,String major_no) {
		
		try {
			adminService.delMajor(major_no);
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","ok");
	        JSONUtil.printByJSON(resp,map);
		} catch (Exception e) {
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","exception");
	        JSONUtil.printByJSON(resp,map);
		}
	}
	
	@ResponseBody
	@RequestMapping("selAllUnit")
	public String selAllUnit(Unit unit) {
		List<Unit> ls = adminService.selAllUnit(unit);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("insUnit")
	public String insUnit(Unit unit) {
		adminService.insUnit(unit);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(unit);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("updUnit")
	public String updUnit(Unit unit) {
		adminService.updUnit(unit);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(unit);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("delUnit")
	public String delUnit(String unit_no) {
		adminService.delUnit(unit_no);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selAllClass")
	public String selAllClass(Admin admin,HttpServletRequest req) {

		List<Admin> ls = adminService.selAllClass(admin);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("insClass")
	public String insClass(Cla cla) {
		adminService.insClass(cla);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(cla);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("updClass")
	public String updClass(String newName, String oldName) {
		adminService.updClass(newName, oldName);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("delClass")
	public String delClass(String class_name) {
		adminService.delClass(class_name);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selAllProto")
	public String selAllProto(Admin admin) {
		List<Admin> ls = adminService.selAllProto(admin);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("updProtoPass")
	public String updProtoPass(String s_no) {
		adminService.updProtoPass(s_no);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("updProtoNoPass")
	public String updProtoNoPass(String s_no) {
		adminService.updProtoNoPass(s_no);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping("selAllStu")
	public String selAllStu(HttpServletRequest req, Admin admin) {
		List<Admin> ls = adminService.selAllStu(admin);
	
//		Iterator<Admin> iterator = ls.iterator();
//		while (iterator.hasNext()) {
//			Admin next = iterator.next();
//			System.out.println(next.getS_name());
//		}
		req.setAttribute("allstu", ls);
//		ObjectMapper mapper = new ObjectMapper();
//		String result = "";
//		try {
//		    result = mapper.writeValueAsString(ls);
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
		
		return "admGLStuMsg";
	}
	
	@ResponseBody
	@RequestMapping("insStu")
	public String insStu(Student student) {
		adminService.insStu(student);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(new Academy());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	 
	
	 
	@RequestMapping("admUpdStu")
	public void admUpdStu(HttpServletResponse resp,Admin a) {
		try {
			adminService.admUpdStu(a);
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","suess");
	        JSONUtil.printByJSON(resp,map);
		} catch (Exception e) {
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","exception");
	        JSONUtil.printByJSON(resp,map);
		}
	}
	
	@ResponseBody
	@RequestMapping("delStu")
	public String delStu(String stu_email) {
		System.out.println(stu_email);
		adminService.delStu(stu_email);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString("ok");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selAllEmploy")
	public String selAllEmploy(Admin admin) {
		List<Admin> ls = adminService.selAllEmploy(admin);
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping("uploadMsg")
	public String upload(Student student, MultipartFile img, HttpServletRequest req) {
		User u = (User) req.getSession().getAttribute("user");
		student.setS_email(u.getUser_email());;
		long size = img.getSize();
		if(size != 0l) {
			String fileName = UUID.randomUUID().toString() + img.getOriginalFilename().substring(img.getOriginalFilename().lastIndexOf("."));
			String path = req.getSession().getServletContext().getRealPath("image");
			try {
			    FileUtils.copyInputStreamToFile(img.getInputStream(), new File(path + "/" + fileName));
			} catch (Exception e) {
				e.printStackTrace();
			}
			student.setProtocoal_img(fileName);
		}
		if(student.getJob_state().equals("已就业")) {
			student.setJob_state("其他");
		}
		adminService.upload(student);
		
		return "redirect:/selCompany";
	}
	
	@RequestMapping("showProto")
	public String proto(HttpServletRequest req, String img, String s_no) {
		req.setAttribute("img", img);
		req.setAttribute("s_no", s_no);
		
		return "showProto";
	}
	
}
