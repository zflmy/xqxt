package com.xqxt.handler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xqxt.entity.Company;
import com.xqxt.entity.JOC;
import com.xqxt.entity.Notice;
import com.xqxt.entity.Occupate;
import com.xqxt.entity.RC;
import com.xqxt.entity.User;
import com.xqxt.service.CompanyService;
import com.xqxt.service.JobService;
import com.xqxt.service.ResumeService;
import com.xqxt.utils.JSONUtil;
import com.xqxt.utils.ResumeUtil;
import com.xqxt.utils.TextUtil;

@Controller
public class CompanyHandler {

	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private JobService jobService;
	
	
	
	
	@RequestMapping("companyMsg")
	public String companyMsg(HttpServletRequest req,String com_email) {
		Company com = companyService.selCompanyByNo(com_email);
		System.out.println("+++++++++++"+com.getPhone());
		req.setAttribute("com", com);
		req.setAttribute("c_email", com_email);
		return "companyMsg";
	}
	
	
	@RequestMapping("stuToComResume")
	public String stuToComResume(HttpServletRequest req,String stu_email) {
		List<Company> ls = companyService.selCompany();
		for (Company com : ls) {
			com.setCompany_intro(TextUtil.textFormat(com.getCompany_intro(), 120));
		}
		req.setAttribute("companyLs", ls);
		req.setAttribute("stu_email", stu_email);
		return "stuToComResume";
	}
	
	
	@RequestMapping("selOccupateByNo")
	public String selOccupateByNo(HttpServletRequest req, String c_email,String stu_email) {
		List<Occupate> ls = companyService.selOccupateByNo(c_email);
		List<JOC> joc = jobService.selJobByNo(c_email);
		req.setAttribute("occupateLs", ls);
		req.setAttribute("c_email", c_email);
		req.setAttribute("stu_email", stu_email);
		req.setAttribute("jocLs", joc);
		
		return "jobMain";
	}
	
	@RequestMapping("selCompanyByNo")
	public String selCompanyByNo(HttpServletRequest req, String c_email,String stu_email) {
		Company company = companyService.selCompanyByNo(c_email);
		req.setAttribute("company", company);
		req.setAttribute("stu_email", stu_email);
		return "stuLookCompanyMsg";
	}
	
}
