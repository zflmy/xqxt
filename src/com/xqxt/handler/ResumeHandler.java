package com.xqxt.handler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.Company;
import com.xqxt.entity.JOC;
import com.xqxt.entity.Notice;
import com.xqxt.entity.RC;
import com.xqxt.entity.Resume;
import com.xqxt.entity.User;
import com.xqxt.service.ResumeService;
import com.xqxt.utils.JSONUtil;
import com.xqxt.utils.ResumeUtil;
import com.xqxt.utils.TextUtil;
import com.xqxt.utils.TransferUtil;

@Controller
public class ResumeHandler {

	@Autowired
	private ResumeService resumeService;

	
	private boolean isHaveEmail(RC rc) {
		List<RC> allRC = resumeService.selAllRC();
		Iterator<RC> allRCIterator = allRC.iterator();
		boolean isHaveEmail=false;
		while (allRCIterator.hasNext()){
			RC next = allRCIterator.next();
			if(next.getC_email().equals(rc.getC_email())&&next.getS_email().equals(rc.getS_email())&&next.getJob_no().equals(rc.getJob_no())) {
				isHaveEmail = true;
				
			}
		}
		return isHaveEmail;
	}
//	@RequestMapping("editResume")
//	public String editResume(HttpServletRequest req) {
//		User user = (User) req.getSession().getAttribute("user");
//		List<JOC> selRCMsg = resumeService.selRCMsg(user.getUser_email());
//		
//		req.setAttribute("selRCMsg", selRCMsg);
//		
//		return "editResume";
//	}
	@RequestMapping("stuJobProgress")
	public String stuJobProgress(HttpServletRequest req, String stu_email) {
		List<JOC> selRCMsg = resumeService.selRCMsg(stu_email);
		
		req.setAttribute("selRCMsg", selRCMsg);
		
		return "stuJobProgress";
	}
	@RequestMapping("insRC")
	public void insRC(HttpServletResponse resp,RC rc) {
		if(isHaveEmail(rc)) {
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","exit");
	        JSONUtil.printByJSON(resp,map);
		}
		rc.setState_msg(ResumeUtil.getState(1));
		resumeService.insRC(rc);
		
		HashMap<String, Object> map = new HashMap<String,Object>();
        map.put("result","suess");
        JSONUtil.printByJSON(resp,map);
	}
	
	
	@RequestMapping("selRC")
	public String selRC(HttpServletRequest req,String com_email) {
		List<JOC> ls = resumeService.selRC(com_email);
		Iterator<JOC> iterator = ls.iterator();
		while(iterator.hasNext()) {
			JOC next = iterator.next();
			if(next.getState_msg().equals(ResumeUtil.getState(5))) {
				iterator.remove();
			}
		}
		req.setAttribute("rcLs", ls);
		req.setAttribute("rcLs_count", ls.size());
		return "comLookResume";
	}
	
	@ResponseBody
	@RequestMapping("updRC")
	public void updRC(HttpServletResponse resp,RC rc, int state) {
		if(state==2) {//如果是查看简历状态
			List<RC> allRC = resumeService.selAllRC();
			Iterator<RC> iterator = allRC.iterator();
			while(iterator.hasNext()) {
				RC next = iterator.next();
				if(next.getC_email().equals(rc.getC_email())&&next.getS_email().equals(rc.getS_email())&&next.getJob_no().equals(rc.getJob_no())) {
					if(next.getState_msg().equals(ResumeUtil.getState(3))||next.getState_msg().equals(ResumeUtil.getState(4))) {
						HashMap<String, Object> map = new HashMap<String,Object>();
			            map.put("result","unChangeState");
			            JSONUtil.printByJSON(resp,map);
					}else {
						rc.setState_msg(ResumeUtil.getState(state));
						resumeService.updRC(rc);
						HashMap<String, Object> map = new HashMap<String,Object>();
				        map.put("result","ok");
				        JSONUtil.printByJSON(resp,map);
					}
				}
			}
		}
		if(state==3||state==4||state==5) {
			rc.setState_msg(ResumeUtil.getState(state));
			resumeService.updRC(rc);
			HashMap<String, Object> map = new HashMap<String,Object>();
	        map.put("result","ok");
	        JSONUtil.printByJSON(resp,map);
		
		}
		
	}
	
	@RequestMapping("editResume")
	public String editResume(HttpServletRequest req,String stu_email) {
		
		req.setAttribute("resume", resumeService.selResume(stu_email));
		
		return "editResume";
	}
	
	@RequestMapping("insResume")
	public String insResume(HttpServletRequest req,Resume resume) {
		resumeService.updResume(resume);
		req.setAttribute("msg", "保存成功");
		req.setAttribute("resume", resumeService.selResume(resume.getS_email()));
		return "editResume";
		
	}
	
	@RequestMapping("selResume")
	public String selResume(HttpServletRequest req, String s_email) {
		Resume resume = resumeService.selResume(s_email);
		req.setAttribute("resume", resume);
		return "resume";
	}
	
}
