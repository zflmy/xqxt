package com.xqxt.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xqxt.entity.Academy;
import com.xqxt.entity.User;
import com.xqxt.service.AcademyService;

@Controller
public class AcademyHandler {

	@Autowired
	private AcademyService academyService;
	
	@RequestMapping("selAcademy")
	public String selAcademy(HttpServletRequest req) {
		List<Academy> ls = academyService.selAcademy();
        req.setAttribute("academy", ls);

        User user = (User) req.getSession().getAttribute("user");
        
        if(user.getUser_type().equals("ѧ��")) return "stuMsg";
        return "admin";
	}
	
}
