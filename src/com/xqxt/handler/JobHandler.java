package com.xqxt.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqxt.entity.JOC;
import com.xqxt.service.JobService;
import com.xqxt.utils.TransferUtil;

@Controller
public class JobHandler {

	@Autowired
	private JobService jobService;
	
	@ResponseBody
	@RequestMapping("selJobByOccupate")
	public String selJob(HttpServletRequest req, String c_email, String occupate_no) {
		if(occupate_no == null) {
			occupate_no = "1=1";
		}
		List<JOC> ls = jobService.selJob(c_email, occupate_no);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping("selJobByNo")
	public String selJobByNo(HttpServletRequest req, String c_email) {
		List<JOC> ls = jobService.selJobByNo(c_email);
		
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
		    result = mapper.writeValueAsString(ls);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping("selJocByNo")
	public String selJocByNo(HttpServletRequest req, String job_no,String c_email,String stu_email) {
		JOC joc = jobService.selJocByNo(c_email, job_no);
		joc.setJob_required(TransferUtil.getText(joc.getJob_required()));
		req.setAttribute("joc", joc);
		req.setAttribute("c_email", c_email);
		req.setAttribute("stu_email", stu_email);
		return "jobMsg";
	}
	
}
