package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Notice;
import com.xqxt.mapper.NoticeMapper;

@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	private NoticeMapper noticeMapper;
	
	@Override
	public void insNotice(Notice notice) {
		noticeMapper.insNotice(notice);
	}

	@Override
	public List<Notice> selNotice(String sendto) {
		return noticeMapper.selNotice(sendto);
	}

	

}
