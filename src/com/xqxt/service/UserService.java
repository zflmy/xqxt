package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.User;

public interface UserService {

	void insUser(User user);
	
	void delUser(String user_name);
	
	void updUser(User user);
	
	User selUser(User user);
	
	List<User> selAll();
	
	void insResumeEmail(String s_email);
}
