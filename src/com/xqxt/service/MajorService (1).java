package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Major;

public interface MajorService {

    void insMajor(Major major);
	
	List<Major> selMajorByAcademy(String academy_no);
	
	List<Major> selALLMajor();
}
