package com.xqxt.service;

import com.xqxt.entity.Student;

public interface StudentService {

	void insStudent(Student student);
	
	Student selStudent(String s_no);
	
}
