package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Count;

public interface ShowService {

    List<Count> selWhole();
	
	List<Count> selByAcademy(String academy_name);
	
	List<Count> selByMajor(String major_no);
	
	List<Count> selAcademys();
	
	List<Count> selMajors(String academy_no);
	
}
