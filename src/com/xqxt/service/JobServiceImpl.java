package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.JOC;
import com.xqxt.mapper.JobMapper;

@Service
public class JobServiceImpl implements JobService {

	@Autowired
	private JobMapper jobMapper;
	
	@Override
	public List<JOC> selJob(String c_email, String occupate_no) {
		return jobMapper.selJob(c_email, occupate_no);
	}

	@Override
	public List<JOC> selJobByNo(String c_email) {
		return jobMapper.selJobByNo(c_email);
	}

	@Override
	public JOC selJocByNo(String c_email, String job_no) {
		return jobMapper.selJocByNo(c_email, job_no);
	}

}
