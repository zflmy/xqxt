package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Academy;
import com.xqxt.mapper.AcademyMapper;

@Service
public class AcademyServiceImpl implements AcademyService {

	@Autowired
	private AcademyMapper academyMapper;
	
	@Override
	public void insAcademy(Academy academy) {
		academyMapper.insAcademy(academy);
	}

	@Override
	public List<Academy> selAcademy() {
		return academyMapper.selAcademy();
	}

}
