package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Major;
import com.xqxt.mapper.MajorMapper;

@Service
public class MajorServiceImpl implements MajorService {

	@Autowired
	private MajorMapper majorMapper;
	
	@Override
	public void insMajor(Major major) {
		majorMapper.insMajor(major);
	}

	@Override
	public List<Major> selMajorByAcademy(String academy_no) {
		return majorMapper.selMajorByAcademy(academy_no);
	}

	@Override
	public List<Major> selALLMajor() {
		// TODO Auto-generated method stub
		return majorMapper.selALLMajor();
	}

}
