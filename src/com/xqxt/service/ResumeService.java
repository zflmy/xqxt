package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.JOC;
import com.xqxt.entity.RC;
import com.xqxt.entity.Resume;

public interface ResumeService {

    void insRC(RC rc);
	
	List<JOC> selRC(String c_email);
	
	List<JOC> selRCMsg(String s_email);
	
	List<RC> selAllRC();
	
	void updRC(RC rc);
	
	void updResume(Resume resume);
	
    void insResume(Resume resume);
	
	Resume selResume(String s_email);
	
}
