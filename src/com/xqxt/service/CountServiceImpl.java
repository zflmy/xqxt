package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Admin;
import com.xqxt.entity.Result;
import com.xqxt.mapper.CountMapper;

@Service
public class CountServiceImpl implements CountService {

	@Autowired
	private CountMapper countMapper;
	
	@Override
	public int selAllNum(Admin admin) {
		return countMapper.selAllNum(admin);
	}

	@Override
	public List<Result> selState(Admin admin) {
		return countMapper.selState(admin);
	}

	@Override
	public List<Result> selType(Admin admin) {
		return countMapper.selType(admin);
	}

	@Override
	public int selByType(Admin admin) {
		return countMapper.selByType(admin);
	}

}
