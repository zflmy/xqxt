package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Academy;
import com.xqxt.entity.Admin;
import com.xqxt.entity.Cla;
import com.xqxt.entity.Major;
import com.xqxt.entity.Student;
import com.xqxt.entity.Unit;

public interface AdminService {

	List<Academy> selAllAcademy();
	
	void insAcademy(Academy academy);
	
	void updAcademy(String oldAcademy_no,String academy_no,String academy_name);
	
	void delAcademy(String academy_no);
	
	List<Admin> selAllMajor(Admin admin);
	
	void insMajor(Major major);
	
    void updMajor(String oldMajor_no,String academy_no,String major_no,String major_name);
	
	void delMajor(String major_no);
	
	List<Unit> selAllUnit(Unit unit);
	
	void insUnit(Unit unit);
	
	void updUnit(Unit unit);
	
	void delUnit(String unit_no);
	
    List<Admin> selAllClass(Admin admin);
	
	void insClass(Cla cla);
	
	void updClass(String newName, String oldName);
	
	void delClass(String class_name);
	
    List<Admin> selAllProto(Admin admin);
	
	void updProtoPass(String s_no);
	
	void updProtoNoPass(String s_no);
	
    List<Admin> selAllStu(Admin admin);
	
	void insStu(Student student);
	
	void admUpdStu(Admin admin);
	
	void delStu(String s_no);
	
	List<Admin> selAllEmploy(Admin admin);
	
	void upload(Student student);
	
}
