package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Appoint;

public interface AppointService {

    void insAppoint(Appoint appoint);
	
	List<Appoint> selAppoint();
	
	void updAppoint(Appoint appoint);
	
	Appoint selOneAppoint(String c_email);
}
