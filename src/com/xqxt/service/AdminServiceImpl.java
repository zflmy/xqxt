package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Academy;
import com.xqxt.entity.Admin;
import com.xqxt.entity.Cla;
import com.xqxt.entity.Major;
import com.xqxt.entity.Student;
import com.xqxt.entity.Unit;
import com.xqxt.mapper.AdminMapper;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminMapper adminMapper;
	
	@Override
	public List<Academy> selAllAcademy() {
		return adminMapper.selAllAcademy();
	}

	@Override
	public void insAcademy(Academy academy) {
		adminMapper.insAcademy(academy);
	}

	@Override
	public void updAcademy(String oldAcademy_no,String academy_no,String academy_name) {
		adminMapper.updAcademy(oldAcademy_no,academy_no,academy_name);
	}

	@Override
	public void delAcademy(String academy_no) {
		adminMapper.delAcademy(academy_no);
	}

	@Override
	public List<Admin> selAllMajor(Admin admin) {
		return adminMapper.selAllMajor(admin);
	}

	@Override
	public void insMajor(Major major) {
		adminMapper.insMajor(major);
	}

	@Override
	public void updMajor(String oldMajor_no,String academy_no,String major_no,String major_name) {
		adminMapper.updMajor(oldMajor_no,academy_no,major_no,major_name);
	}

	@Override
	public void delMajor(String major_no) {
		adminMapper.delMajor(major_no);
	}

	@Override
	public List<Unit> selAllUnit(Unit unit) {
		return adminMapper.selAllUnit(unit);
	}

	@Override
	public void insUnit(Unit unit) {
		adminMapper.insUnit(unit);
	}

	@Override
	public void updUnit(Unit unit) {
		adminMapper.updUnit(unit);
	}

	@Override
	public void delUnit(String unit_no) {
		adminMapper.delUnit(unit_no);
	}

	@Override
	public List<Admin> selAllClass(Admin admin) {
		return adminMapper.selAllClass(admin);
	}

	@Override
	public void insClass(Cla cla) {
		adminMapper.insClass(cla);
	}

	@Override
	public void updClass(String newName, String oldName) {
		adminMapper.updClass(newName, oldName);
	}

	@Override
	public void delClass(String class_name) {
		adminMapper.delClass(class_name);
	}

	@Override
	public List<Admin> selAllProto(Admin admin) {
		return adminMapper.selAllProto(admin);
	}

	@Override
	public void updProtoPass(String s_no) {
		adminMapper.updProtoPass(s_no);
	}

	@Override
	public void updProtoNoPass(String s_no) {
		adminMapper.updProtoNoPass(s_no);
	}

	@Override
	public List<Admin> selAllStu(Admin admin) {
		return adminMapper.selAllStu(admin);
	}

	@Override
	public void insStu(Student student) {
		adminMapper.insStu(student);
	}

	@Override
	public void admUpdStu(Admin admin) {
		adminMapper.admUpdStu(admin);
	}

	@Override
	public void delStu(String s_no) {
		adminMapper.delStu(s_no);
	}

	@Override
	public List<Admin> selAllEmploy(Admin admin) {
		return adminMapper.selAllEmploy(admin);
	}

	@Override
	public void upload(Student student) {
		adminMapper.upload(student);
	}

}
