package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Admin;
import com.xqxt.entity.Result;

public interface CountService {

	int selAllNum(Admin admin);
	
	List<Result> selState(Admin admin);
	
	List<Result> selType(Admin admin);
	
	int selByType(Admin admin);
	
}
