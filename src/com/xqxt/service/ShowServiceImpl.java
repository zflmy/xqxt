package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Count;
import com.xqxt.mapper.ShowMapper;

@Service
public class ShowServiceImpl implements ShowService {

	@Autowired
	private ShowMapper showMapper;
	
	@Override
	public List<Count> selWhole() {
		return showMapper.selWhole();
	}

	@Override
	public List<Count> selByAcademy(String academy_name) {
		return showMapper.selByAcademy(academy_name);
	}

	@Override
	public List<Count> selByMajor(String major_no) {
		return showMapper.selByMajor(major_no);
	}

	@Override
	public List<Count> selAcademys() {
		return showMapper.selAcademys();
	}

	@Override
	public List<Count> selMajors(String academy_no) {
		return showMapper.selMajors(academy_no);
	}

}
