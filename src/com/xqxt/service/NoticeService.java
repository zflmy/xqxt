package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Notice;

public interface NoticeService {

    void insNotice(Notice notice);
	
	List<Notice> selNotice(String sendto);
	
}
