package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Academy;

public interface AcademyService {

    void insAcademy(Academy academy);
	
	List<Academy> selAcademy();
	
}
