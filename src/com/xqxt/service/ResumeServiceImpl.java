package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.JOC;
import com.xqxt.entity.RC;
import com.xqxt.entity.Resume;
import com.xqxt.mapper.ResumeMapper;

@Service
public class ResumeServiceImpl implements ResumeService {

	@Autowired
	private ResumeMapper resumeMapper;
	
	@Override
	public void insRC(RC rc) {
		resumeMapper.insRC(rc);
	}

	@Override
	public List<JOC> selRC(String c_email) {
		return resumeMapper.selRC(c_email);
	}

	@Override
	public void insResume(Resume resume) {
		resumeMapper.insResume(resume);
	}

	@Override
	public Resume selResume(String s_email) {
		return resumeMapper.selResume(s_email);
	}

	@Override
	public List<JOC> selRCMsg(String s_email) {
		return resumeMapper.selRCMsg(s_email);
	}

	@Override
	public void updRC(RC rc) {
		resumeMapper.updRC(rc);
	}

	@Override
	public List<RC> selAllRC() {
		return resumeMapper.selAllRC();
	}

	@Override
	public void updResume(Resume resume) {
		resumeMapper.updResume(resume);
	}

}
