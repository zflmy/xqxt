package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.User;
import com.xqxt.mapper.UserMapper;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public void insUser(User user) {
		userMapper.insUser(user);
	}

	@Override
	public void delUser(String user_name) {
		userMapper.delUser(user_name);
	}

	@Override
	public void updUser(User user) {
		userMapper.updUser(user);
	}

	@Override
	public User selUser(User user) {
		
		return userMapper.selUser(user);
	}

	@Override
	public List<User> selAll() {
		return userMapper.selAll();
	}

	@Override
	public void insResumeEmail(String s_email) {
		userMapper.insResumeEmail(s_email);
	}


	
}
