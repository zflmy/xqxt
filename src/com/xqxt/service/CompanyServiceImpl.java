package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Company;
import com.xqxt.entity.JC;
import com.xqxt.entity.Occupate;
import com.xqxt.mapper.CompanyMapper;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyMapper companyMapper;
	
	@Override
	public List<Company> selCompany() {
		return companyMapper.selCompany();
	}

	@Override
	public List<JC> selJobByCompany(JC jc) {
		return companyMapper.selJobByCompany(jc);
	}

	@Override
	public List<JC> selJobByJob(String job_name) {
		return companyMapper.selJobByJob(job_name);
	}

	@Override
	public Company selCompanyByNo(String c_email) {
		return companyMapper.selCompanyByNo(c_email);
	}

	@Override
	public List<Occupate> selOccupateByNo(String c_email) {
		return companyMapper.selOccupateByNo(c_email);
	}

}
