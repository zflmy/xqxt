package com.xqxt.service;

import java.util.List;

import com.xqxt.entity.Company;
import com.xqxt.entity.JC;
import com.xqxt.entity.Occupate;

public interface CompanyService {

	List<Company> selCompany();
	
	List<Occupate> selOccupateByNo(String c_email);
	
	Company selCompanyByNo(String c_email);
	
	List<JC> selJobByCompany(JC jc);
	
	List<JC> selJobByJob(String job_name);
	
}
