package com.xqxt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Student;
import com.xqxt.mapper.StudentMapper;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentMapper studentMapper;
	
	@Override
	public void insStudent(Student student) {
		studentMapper.insStudent(student);
	}

	@Override
	public Student selStudent(String s_no) {
		return studentMapper.selStudent(s_no);
	}

}
