package com.xqxt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xqxt.entity.Appoint;
import com.xqxt.mapper.AppointMapper;

@Service
public class AppointServiceImpl implements AppointService {

	@Autowired
	private AppointMapper appointMapper;

	@Override
	public void insAppoint(Appoint appoint) {
		appointMapper.insAppoint(appoint);
	}

	@Override
	public List<Appoint> selAppoint() {
		return appointMapper.selAppoint();
	}

	@Override
	public Appoint selOneAppoint(String c_email) {
		return appointMapper.selOneAppoint(c_email);
	}

	@Override
	public void updAppoint(Appoint appoint) {
		appointMapper.updAppoint(appoint);
		
	}
	
	

}
