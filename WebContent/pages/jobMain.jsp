<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>jobMain</title>

    <link rel="shortcut icon" href="favicon.ico"> <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
	<input type="hidden"  id="c_email" value="${c_email}" />
	<input type="hidden"  id="stu_email" value="${stu_email}" />
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <div class="space-25"></div>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li>
                                    <a href=javascript:find()>  <span style="font-size: 20px;">全部</span> 
                                    </a>
                                </li>
                                 <c:forEach items="${occupateLs }" var="occupate">
                       				 <li><a href=javascript:search('${occupate.occupate_no}')> <span style="font-size: 15	px;">${occupate.occupate_name }</span> </a> </li>
                   				 </c:forEach>
                                
                            </ul>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 animated fadeInRight" >
                <div class="mail-box-header" style="margin-bottom: 50px;">

                    <form method="get" action="#" class="pull-right mail-search">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" name="search" placeholder="搜索相关职业">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary">
                                    搜索
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
                <div class="mail-box" id="all_job">
					<table class="table table-hover table-mail">
                        <tbody>
                         <c:forEach items="${jocLs }" var="joc">
                            <tr class="unread">
                               
                                <td ><a href="selJocByNo?job_no=${joc.job_no }&c_email=${c_email}&stu_email=${stu_email}">${joc.job_name }></a>
                                </td>
                                <td >急招 ${joc.need_num } 人
                                </td>
                                <td >薪资待遇${joc.salary }</i>
                                </td>
                            </tr>
                            <tr>
                            	<td  colspan="3"><strong>岗位需求：<a href="selJocByNo?job_no=${joc.job_no }&c_email=${c_email}&stu_email=${stu_email}">${joc.job_name }></a></span>
                            </tr>
                            <tr>
                            	<td  colspan="3"></a>
                            </tr>
                          </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="jstatic/js/plugins/iCheck/icheck.min.js"></script>
    <script>
    
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    
    	 function search(occupate_no) {
    	 var c_email = $("#c_email").val();
    	 var stu_email = $("#stu_email").val();
        	rs = "";
        	$.ajax({
                url : "selJobByOccupate",
                data : {"c_email" : c_email, "occupate_no" : occupate_no},
                dataType : "json",
                success : function(data) {
                	for(var i = 0; i < data.length; i ++) {
                	rs+="<table class='table table-hover table-mail'>"+
                        "<tbody >"+
                	"<c:forEach items='${jocLs }' var='joc'>"+
                            "<tr class='unread'>"+
                            
                             "<td><a href='selJocByNo?job_no="+data[i].job_no+"&c_email="+c_email+"&stu_email="+stu_email+"'>"+data[i].job_name+"</a>"+
                            "</td>"+
                                "<td>急招 "+data[i].need_num+" 人"+
                                "</td>"+
                                "<td >薪资待遇"+data[i].salary+"</i>"+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                            	"<td colspan='3'><strong>岗位需求：</strong><span><a href='selJocByNo?job_no="+data[i].job_no+"&c_email="+c_email+"&stu_email="+stu_email+"'>"+data[i].job_required+"</a></span></a>"+
                           "</tr>"+
                            "<tr>"+
                            	"<td colspan='3'></a>"+
                           "</tr>"+
                          "</c:forEach>"+
                          "</tbody>"+
                   	 "</table>";
                          
                		
                	}
                	
        			$("#all_job").html(rs);
                },
                type : "post"
            });
        }
        
        function find() {
        	var c_email = $("#c_email").val();
        	var stu_email = $("#stu_email").val();
        	rs = "";
        	$.ajax({
                url : "selJobByNo",
                data : {"c_email" : c_email},
                dataType : "json",
                success : function(data) {
                	for(var i = 0; i < data.length; i ++) {
                		rs+="<table class='table table-hover table-mail'>"+
                        "<tbody >"+
                	"<c:forEach items='${jocLs }' var='joc'>"+
                            "<tr class='unread'>"+
                               
                             "<td class='mail-ontact'><a href='selJocByNo?job_no="+data[i].job_no+"&c_email="+c_email+"&stu_email="+stu_email+"'>"+data[i].job_name+"</a>"+
                            "</td>"+
                                "<td class='mail-subject'>急招 "+data[i].need_num+" 人"+
                                "</td>"+
                                "<td >薪资待遇"+data[i].salary+"</i>"+
                                "</td>"+
                            "</tr>"+
                            "<tr>"+
                            	"<td class='mail-ontact' colspan='3'><strong>岗位需求：</strong><span><a href='selJocByNo?job_no="+data[i].job_no+"&c_email="+c_email+"&stu_email="+stu_email+"'>"+data[i].job_required+"</a></span></a>"+
                           "</tr>"+
                            "<tr>"+
                            	"<td class='mail-ontact' colspan='3'></a>"+
                           "</tr>"+
                          "</c:forEach>"+
                          "</tbody>"+
                   	 "</table>";
                	}
                	
        			$("#all_job").html(rs);
                },
                type : "post"
            });
        }
    </script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/mailbox.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:18:22 GMT -->
</html>
