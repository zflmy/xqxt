<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/article.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:47 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>工作详情</title>

		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<style type="text/css">
	
    
    .com_job_main .job_message{width: 100%; height: 200px;}
    .job_message .job_msg_content{margin: 0 auto; width: 80%; height: 240px; line-height: 65px;}
    .job_msg_content strong{font-size: 20px;  letter-spacing: 3px;}
    .job_msg_content span{font-size: 18px; ; letter-spacing: 1px;}
    
    .com_job_main .com_job_req{width: 100%;}
    .com_job_req .job_req{margin: 70px auto; width: 80%; line-height: 40px;}
    .job_req .job_text strong{font-size: 20px; letter-spacing: 3px;}
    .job_req ul{margin-left: 30px;}
    .job_req ul li{font-size: 18px; }
    
</style>
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content  animated fadeInRight article">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="ibox">
						<div class="ibox-content">
							<div class="text-center article-title">
								<h1>
									${joc.company_name }
								</h1>
								<hr>
							</div>
							<hr>

							<div class="job_message">
								<div class="job_msg_content">
									<strong>岗位名称：</strong><span>${joc.job_name }</span><br />
									<strong>公司位置：</strong><span>${joc.city }</span><br />
									<strong>招聘人数：</strong><span>${joc.need_num }人</span><br />
									<strong>薪资待遇：</strong><span>${joc.salary }</span>
								</div>
							</div>

							<div class="com_job_req">
								<div class="job_req">
									<div class="job_text">
										<strong>岗位需求：</strong>
									</div>
									<ul>
										${joc.job_required }
									</ul>
								</div>
							</div>

							<div class="com_job_btn" style=" text-align: center;">
								<div class="deliver">
								<button type="button" class="btn btn-primary btn-lg" onclick="submit()">投&emsp;&emsp;递</button>
								</div>
							</div>



						</div>

					</div>
				</div>
			</div>

		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
	 <script type="text/javascript">
 
        function submit() {
       
        	c_email = "${joc.c_email}";
        	s_email = "${stu_email}";
        	job_no = "${joc.job_no}";
        	$.ajax({
                type: "POST",
                url: "insRC",
                data: {"c_email":c_email,"s_email":s_email,"job_no":job_no},
                async: false,
                success: function(msg){
                	 if(msg["result"]=="suess"){
                       alert("投递成功");
                    }
                    else if(msg["result"]=="exit"){
                       alert("已经投递过");
                    }
                   
                }
            });
        }
    </script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/article.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:47 GMT -->
</html>
</html>