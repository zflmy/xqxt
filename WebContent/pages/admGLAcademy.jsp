<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:20:01 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title></title>

		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

		<!-- Data Tables -->
		<link href="${pageContext.request.contextPath}/static/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<style type="text/css">
touch-action: none;
</style>
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>基本 <small>分类，查找</small></h5>

						</div>
						<div class="ibox-content">
							

							<table class="table table-striped table-bordered table-hover dataTables-example" >
								<thead>
									<tr>
										<th>学院编号</th>
										<th>学院名称</th>
										<th>修改</th>
										<th>删除</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${allAcademy}" var="as">
								<tr class="gradeX">
										<td>${as.academy_no}</td>
										<td>${as.academy_name}</td>
										<td><a onclick='changeMsg(this)' ><span class="glyphicon glyphicon-cog" aria-hidden="true" style="color:#1AB394;"></span></a></td>
	                        			<td><a onclick='del(this)' ><span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:#1AB394;"></span></a></td>
									</tr>
                   				 </c:forEach>
									
									
								</tbody>
								<tfoot>
									<tr>
										<th>学院编号</th>
										<th>学院名称</th>
										<th>修改</th>
										<th>删除</th>
									</tr>
								</tfoot>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/jeditable/jquery.jeditable.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/dataTables/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/dataTables/dataTables.bootstrap.js"></script>
		<script src="${pageContext.request.contextPath}/static/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
		<script>
			$(document).ready(function() {
				$(".dataTables-example").dataTable();
				var oTable = $("#editable").dataTable();
				oTable.$("td").editable("http://www.zi-han.net/theme/example_ajax.php", {
					"callback": function(sValue, y) {
						var aPos = oTable.fnGetPosition(this);
						oTable.fnUpdate(sValue, aPos[0], aPos[1])
					},
					"submitdata": function(value, settings) {
						return {
							"row_id": this.parentNode.getAttribute("id"),
							"column": oTable.fnGetPosition(this)[2]
						}
					},
					"width": "90%",
					"height": "100%"
				})
			});

			function del(a) {
				 var tr = $(a).closest('tr'), td = tr.find('td');
				 var academy_no = td.eq(0).html();
				 layer.confirm('确定要删除？', {
		      		  btn: ['确定','取消'] //按钮
		      		}, function(){
		      			 $.ajax({
		         			url : "delAcademy",
		         			data : {"academy_no":academy_no},
		         			dataType : "json",
		         			success : function(data) {
		         				if(data=="ok"){
		         					 layer.msg('删除成功！',{time:1*1000},function(){
			                        	   location.reload();
			  	                	 });
		         				}
		         				else{
		                        	layer.msg('系统错误，删除失败，请重试！',{time:1*1000},function(){
		                         	   location.reload();
		   	                	 	});
		                        }
		         			
		         			},
		         			type : "post"
		         		});
		      			
		      		}, function(){
		      			location.reload();
		      		});
			};
			
			 function changeMsg(a) {
			 	var parentwidth = parseInt(document.body.clientWidth);
			 	var parentheight = parseInt(document.body.clientHeight);
			 	if(parentheight<parseInt(400)){
			 		parentheight=400;
			 	}
			 	var width = parentwidth*0.6+'px';
			 	var height = parentheight*0.6+'px';
       			 var tr = $(a).closest('tr'), td = tr.find('td');
        			//alert(tr[0].rowIndex);
        			//alert(td.eq(0).html());
        			layer.open({
                		type: 2,
               		 	shadeClose: true, //点击遮罩关闭层
               		 	area: [width, height],
                		fixed: false, //不固定
                		maxmin: false,
                		closeBtn: 1,
                		resize: true,
                		skin:  'layui-layer-molv',
                		title: "修改学院信息",
                		content: '${pageContext.request.contextPath}/pages/admChangeAcademy.jsp',
                 		btn: ['关闭'],
                 			// yes: function(index, layero){
                			//     //按钮【按钮一】的回调
                			//     return false
                			//  },
                			// ,btn2: function(index, layero){
                			//     //按钮【按钮二】的回调
                			//     alert("点击了确认");
                			//     //return false 开启该代码可禁止点击该按钮关闭
                		// },
               		 	success: function(layero, index){
                    		var body = layer.getChildFrame('body',index);//建立父子联系
                    		var iframeWin = window[layero.find('iframe')[0]['name']];
                    		var inputList = body.find('.inText');
                    		body.find('#oldAcademy_no').val(td.eq(0).html());
                    		for(var j = 0; j< inputList.length; j++){
                       			 $(inputList[j]).val(td.eq(j).html());
                   			 }

                		},
                		end: function () {//无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。layer.open关闭事件
                    		location.reload();//layer.open关闭刷新
                			}
            			});
        
  			  			}
		</script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>

	</body>


</html>
