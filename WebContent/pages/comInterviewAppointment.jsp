<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>预约面试</title>
<base href="<%=basePath%>">
		

		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/laydate/laydate.css">
		<script src="${pageContext.request.contextPath}/static/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight" style="margin: 2%;">

			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>预约面试</h5>

				</div>
				<div class="ibox-content">
					<form class="form-horizontal m-t" id="signupForm" action="insAppoint" method="post">
						<div class="form-group">
							<label class="col-sm-3 control-label">请选择预约时间</label>
							<div class="col-sm-8">
								<input type="text" name='appoint_time' value='' class="form-control test-item" id="appoint_time" required="" aria-required="true" >
								<span class="help-block m-b-none"  id="tishi"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">预约地点：</label>
							<div class="col-sm-8">
								<input id="appoint_place" name="appoint_place" value='' class="form-control" type="text" required="" aria-required="true" >
							</div>
						</div>
						<br>
						<hr>
						<br>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3" style="text-align: center;">
							<input type="hidden" id="c_email" value="">
						   <input type="hidden" id="job_no" value="">
						   <input type="hidden" id="s_email" value="">
								<input type="button" id="btn_submit" class="btn btn-primary" value="立即预约" />
							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
		
		<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
		<script src="${pageContext.request.contextPath}/static/laydate/laydate.js"></script>
		<script>
		var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			//Demo
			layui.use('form', function() {
				var form = layui.form;
		
				//监听提交
				form.on('submit(formDemo)', function(data) {
					layer.msg(JSON.stringify(data.field));
					return false;
				});
			});
			layui.use('laydate', function(){
			    var laydate = layui.laydate;
			    //同时绑定多个
			    lay('.test-item').each(function(){
			        laydate.render({
			            elem: this
			            ,format:'yyyy-MM-dd,HH:mm:ss'
			            ,type:'datetime'
			            ,trigger: 'click'
			        });
			    });
			});
	
			window.onload = function() { 
    			var $msg = "${msg}";
				if($msg!=""){
					alert($msg);
					$msg="";
				}
			}; 
			
			$("#btn_submit").click(function () {
           	 	var c_email = $("#c_email").val();
           	 	var job_no = $("#job_no").val();
           	 	var s_email = $("#s_email").val();
           	 	var appoint_time = $("#appoint_time").val();
           	 	var appoint_place = $("#appoint_place").val();
            $.ajax({
                type: "POST",
                url: "comIntAppoint",
                data : {"c_email" : c_email, "job_no" : job_no, "s_email" : s_email, "appoint_time" : appoint_time, "appoint_place" : appoint_place},
                async: false,
                success: function(msg){
                	  if(msg["result"]==true){
                		  layer.msg('预约成功！',{time:1*1000},function(){
                			  parent.layer.close(index); 
 	                	 	});
                	  		
                	  }else{
                		  layer.msg('系统错误，请重试！',{time:1*1000},function(){
                			  parent.layer.close(index); 
  	                	 	});
                	  }
                }
            });
            
        	});
			
			 
		</script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:16 GMT -->
</html>
