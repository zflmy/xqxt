<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

<base href="<%=basePath%>">
		<title>我的简历</title>

		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/laydate/laydate.css">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">



			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>修改学院信息</h5>

				</div>
				<div class="ibox-content">
					<form class="form-horizontal m-t" id="signupForm" action="insResume" method="post">
						<input id="oldAcademy_no" type="hidden" value="">
						<div class="form-group">
							<label class="col-sm-3 control-label">学院编号：</label>
							<div class="col-sm-8">
								<input id="academy_no" name="academy_no" value='' class="form-control inText" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">学院名称：</label>
							<div class="col-sm-8">
								<input id="academy_name" name="academy_name" value='' class="form-control inText" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
					
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3">
								<input type="button" onclick="commit()"  class="btn btn-primary" value="提交" />
							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
		
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
		
		<script src="${pageContext.request.contextPath}/static/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
		<script>
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			//Demo
			$(document).ready(function(){
			}); 
			window.onload = function() { 
    			var $msg = "${msg}";
    			
    			var academy_content="";
    			
				if($msg!=""){
					alert($msg);
					$msg="";
				}
			}
			function commit() {
				var oldAcademy_no=$("#oldAcademy_no").val();
				var academy_no=$("#academy_no").val();
				var academy_name=$("#academy_name").val();
				$.ajax({
	        		url : "updAcademy",
	        		data : {"oldAcademy_no" : oldAcademy_no, "academy_no" : academy_no, "academy_name" : academy_name},
	        		dataType : "json",
	        		success : function(data) {
	        			if(data["result"]=="ok"){
	        				layer.msg('修改成功',{time:1*1000},function(){
	              			 	 parent.layer.close(index); 
		                	 });
	        			}else if(data["result"]=="unexit"){
	        				layer.msg('修改的学院编号重复，请更换重试！',{time:1*1000},function(){
	             			 	 parent.layer.close(index); 
		                	 });
	        			}else{
	        				layer.msg('修改失败，请重试！',{time:1*1000},function(){
	             			 	 parent.layer.close(index); 
		                	 });
	        			}
	        				
	        			},
	        			type : "post"
	        		});
			}

		</script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:16 GMT -->
</html>
