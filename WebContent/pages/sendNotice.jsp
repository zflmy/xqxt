<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
<base href="<%=basePath%>">
</head>
<link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<style type="text/css">

</style>
<body class="gray-bg">
    <div class="col-lg-6" style="margin: 10%;">
                <div class="ibox float-e-margins">
                  
                    <div class="ibox-content">
                        <form action="insNotice" method="post" class="form-horizontal m-t" id="commentForm" novalidate="novalidate">
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label">公告内容：</label>
                                <div class="col-sm-8">
                                    <textarea id="ccomment" name="content" placeholder="请输入公告内容..." class="form-control" required="" aria-required="true"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
							<label class="col-sm-3 control-label">发送给：</label>

							<div class="col-sm-8">
								<select class="form-control m-b" value=""  id="record_edu"  name="sendto" required="" aria-required="true">
									<option value ="全部">全部</option>
									<option value ="企业">企业</option>
									<option value ="学生">学生</option>
								</select>

							</div>
						</div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-3">
                                    <button class="btn btn-primary" type="submit">发布</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
               
            </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
	<script>
		window.onload = function() { 
    		var $msg = "${msg}";
			if($msg!=""){
				alert($msg);
			}
		}; 
	</script>
</body>

</html>