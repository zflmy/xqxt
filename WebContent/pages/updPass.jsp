
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="<%=basePath%>">
    <title>修改密码</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body>

<div class="ibox float-e-margins">

    <div class="ibox-title" style="text-align: center">
        <h2>修改密码</h2>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>

        </div>
    </div>
    <div class="ibox-content">
        <form action="updPass" method="post" class="form-horizontal m-t" id="signupForm" novalidate="novalidate" >
       		<input type="hidden" name="stu_email" id="stu_email" value="" />
       		<div class="form-group">
       		    <label class="col-sm-3 control-label">旧密码：</label>
       		    <div class="col-sm-8">
       		        <input id="" name="user_pass" class="form-control" type="password">
       		    </div>
       		</div>
            <div class="form-group">
                <label class="col-sm-3 control-label">新密码：</label>
                <div class="col-sm-8">
                    <input id="password" name="new_pass" class="form-control password_check" type="password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">确认密码：</label>
                <div class="col-sm-8">
                    <input id="confirm_password" name="confirm_password" class="form-control" type="password">
                    <span class="help-block m-b-none" id="passtell"><i class="fa fa-info-circle"></i> 请再次输入您的密码</span>
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox" id="agree" name="agree"> 我已经认真阅读并同意<a href="#">《学企新途使用协议》</a>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <button class="btn btn-primary" id="btn_submit" type="submit">修改</button>
                    <button class="btn btn-primary" id="back" type="button" onclick=javascrtpt:jump()>返回首页</button>
                </div>
            </div>
            
        </form>
    </div>
</div>

<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
<script>
 	function jump(){
        window.parent.location.href="login.jsp";
    }
	window.onload = function() { 
		var $stu_email = $("#stu_email",window.parent.document).val();
		$("#stu_email").val($stu_email);
		if("${updPassMsg}"!=""){
			alert("${updPassMsg}");
			if("${updPassMsg}"=="修改成功"){
				jump();
			}
		}
	}
   

</script>

</body>
</html>

