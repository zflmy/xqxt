<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
</head>

<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<style type="text/css">
	*{margin: 0; padding: 0;}
    .page_top{position: relative; width: 100%; height: 120px; background: #fff;}
    .page_top img{position: absolute; height: 100px; top: 10px; left: 100px;}
    .page_top span{position: absolute; top: 40px; left: 240px; font-size: 24px; color: #00f; letter-spacing: 3px;}

    #top_split{margin: 0px auto; width: 92%; border: none; background: #00f; height: 5px;}

    .stu_main{margin: 50px auto; width:70%; height: 650px; background: #ccc;}
    .stu_main .stu_title{position: relative; top: 30px; left: 15%; width: 70%; height: 50px; line-height: 50px; text-align: center;}
    .stu_title span{font-size: 25px; color: #00f; letter-spacing: 5px; text-shadow: 3px 3px 2px #f00;}
    .stu_main .stu_content{position: relative; left: 10%; top: 70px; width: 80%; height: 460px;}
    .stu_content .stu_article{position: relative; text-align: center; line-height: 66px; top: 20px; left: 20%; width: 60%; height: 420px;}
    .stu_article span{font-size: 20px; color: #00f;}
    .stu_article .field{width: 250px; height: 30px; border: none; text-align: center; font-size: 18px; border-bottom: 1px solid #00f;}
    .stu_article .op{-moz-appearance: none; background: #ccc; width: 250px; height: 30px; border: none; text-align: center; font-size: 18px; border-bottom: 1px solid #00f;}
    .stu_article .file{width: 250px; height: 30px; border: none; text-align: center; font-size: 18px; border-bottom: 1px solid #00f;}
    .show{display: none;}
    .btn{border: none; border-radius: 6px; width: 180px; height: 40px; background: #a00; color: #aaa; font-size: 18px; font-weight: bold;}
    .btn:hover{cursor: pointer; background: #f00; color: #fff;}
</style>

<body>
    <div class="page_top">
        <img src="image/logo.jpg"/>
        <span>学企新途</span>
    </div>
    <hr id="top_split"/>
    
    <div class="stu_main">
        <div class="stu_title">
            <span>学生毕业就业情况调查表</span>
        </div>
        <div class="stu_content">
            <div class="stu_article">
                <form action="uploadMsg" method="post" enctype="multipart/form-data">
                    <span>邮&emsp;&emsp;箱</span><b>：</b> <input type='text' class='field' value='${user.user_email }' name='s_email' disabled="disabled"/><br/>
                    <span>就业情况</span><b>：</b><select class='op employ_op' name='job_state'>
                        <option>请选择</option>
                        <option>已就业</option>
                        <option>考研</option>
                        <option>出国留学</option>
                        <option>其他</option>
                    </select><br/>
                    <span class='show'>单位名称</span><b class='show'>：</b><select class='op show unit_op' name='unit_no'>
                        <option value="">未选择</option>
                    </select><br/>
                    <span class='show'>协议上传</span><b class='show'>：</b><input class='file show' type='file' name='img'/><br/>
                    <input class='btn' type='submit' value='确&emsp;&emsp;定'/>
                </form>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(function() {
        	var unit_content = "";
        	$.ajax({
    	        url : "selAllUnit",
    	        data : {"unit_type" : ""},
    	        dataType : "json",
    	        success : function(data) {
    	        	unit_content += "<option value=''>未选择</option>";
    	        	for(var i = 0; i < data.length; i ++) {
    	        	    unit_content += "<option value='"+data[i].unit_no+"'>"+data[i].unit_name+"</option>"
    	        	}
    	        	$(".unit_op").html(unit_content);
    	        },
    	        type : "post"
    	    });
        });
        
        $(".employ_op").change(function() {
        	var type = $(this).val();
        	if(type == '已就业') {
        		$(".show").css({"display" : "inline"});
        	}
        });
    </script>
</body>
</html>