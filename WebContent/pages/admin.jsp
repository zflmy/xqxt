<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:16:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>学企新途-管理员</title>

<base href="<%=basePath%>">


    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span><img alt="image" class="img-circle" src="${pageContext.request.contextPath}/static/img/profile_small.jpg" /></span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                               <span class="block m-t-xs"><strong class="font-bold">学企新途</strong></span>
                                <span class="text-muted text-xs block">超级管理员<b class="caret"></b></span>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="J_menuItem" href="#">修改头像</a>
                                </li>
                                <li><a class="J_menuItem" href="${pageContext.request.contextPath}/pages/updPass.jsp?stu_email=${user_email}">修改密码</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="login.jsp">安全退出</a>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element"> 
							<a class="navbar-minimalize  btn btn-primary " ><i class="fa fa-bars"></i> </a>
							
                        </div>
                    </li>
					<!--  <li>
					    <a  class="J_menuItem" href="layouts.html">
					        <i class="fa fa-home"></i>
					        <span class="nav-label">就业统计展示</span>
					    </a>
					</li> --!>
					 <li>
					    <a class="J_menuItem" href="admLookComAppoint">
							<i class="fa fa-columns"></i> 
							<span class="nav-label">企业预约信息</span>
						</a>
					</li>
					 <li>
					    <a class="J_menuItem" href="${pageContext.request.contextPath}/pages/sendNotice.jsp">
							<i class="fa fa-columns"></i> 
							<span class="nav-label">发布公告</span>
						</a>
					</li>
					 <li>
					    <a class="J_menuItem" href="${pageContext.request.contextPath}/pages/admAddUser.jsp">
							<i class="fa fa-columns"></i> 
							<span class="nav-label">添加用户信息</span> 
						</a>
					</li>
                    <li>
                        <a href="#">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">信息管理</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="selAllStu" >学生信息管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="selAllAcademyToPage">学院信息管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="selAllMajorToPage">专业信息管理</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="selAllClassToPage">班级信息管理</a>
                            </li>
                           <!--
                            <li>
                                <a href="index_v5.html" class="J_menuItem">就业协议审核</a>
                            </li>
                            -->
							<li>
							    <a class="J_menuItem" href="admSelAllCom">单位信息管理</a>
							</li>
							<li>
							    <a href="selAllProtoToPage" class="J_menuItem">学生就业管理</a>
							</li>
                        </ul>

                    </li>
                  
                  </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
      <!--右侧部分开始-->
      <div id="page-wrapper" class="gray-bg dashbard-1">
      
      	<div class="row content-tabs">
      		<button onclick="history.go(-1)" class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
      		</button>
      		<nav class="page-tabs J_menuTabs">
      			<div class="page-tabs-content">
      				<a href="javascript:;" class="active J_menuTab" data-id="admLookComAppoint">就业统计展示</a>
      			</div>
      		</nav>
      		<button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
      		</button>
      		<div class="btn-group roll-nav roll-right">
      			<button class="dropdown J_tabClose right-sidebar-toggle" data-toggle="dropdown">主题<span class="caret"></span>
      
      			</button>
      
      		</div>
      		<a href="login.jsp" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
      	</div>
      	<div class="row J_mainContent" id="content-main" style="height:100%">
      		<iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="admLookComAppoint?v=4.0" frameborder="0"
      		 data-id="admLookComAppoint" seamless></iframe>
      	</div>
      	<!-- <div class="footer">
          <div class="pull-right">&copy; 2014-2015 <a href="http://www.zi-han.net/" target="_blank">zihan's blog</a>
          </div>
      </div> -->
      </div>
      <!--右侧部分结束-->
      <!--右侧边栏开始-->
      <div id="right-sidebar">
      	<div class="sidebar-container">
      		<div class="tab-content">
      			<div id="tab-1" class="tab-pane active">
      				<div class="sidebar-title">
      					<h3> <i class="fa fa-comments-o"></i> 主题设置</h3>
      					<small><i class="fa fa-tim"></i> 你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small>
      				</div>
      				<div class="skin-setttings">
      					<div class="title">主题设置</div>
      					<div class="setings-item">
      						<span>收起左侧菜单</span>
      						<div class="switch">
      							<div class="onoffswitch">
      								<input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="collapsemenu">
      								<label class="onoffswitch-label" for="collapsemenu">
      									<span class="onoffswitch-inner"></span>
      									<span class="onoffswitch-switch"></span>
      								</label>
      							</div>
      						</div>
      					</div>
      					<div class="setings-item">
      						<span>固定顶部</span>
      
      						<div class="switch">
      							<div class="onoffswitch">
      								<input type="checkbox" name="fixednavbar" class="onoffswitch-checkbox" id="fixednavbar">
      								<label class="onoffswitch-label" for="fixednavbar">
      									<span class="onoffswitch-inner"></span>
      									<span class="onoffswitch-switch"></span>
      								</label>
      							</div>
      						</div>
      					</div>
      					<div class="setings-item">
      						<span>
      							固定宽度
      						</span>
      
      						<div class="switch">
      							<div class="onoffswitch">
      								<input type="checkbox" name="boxedlayout" class="onoffswitch-checkbox" id="boxedlayout">
      								<label class="onoffswitch-label" for="boxedlayout">
      									<span class="onoffswitch-inner"></span>
      									<span class="onoffswitch-switch"></span>
      								</label>
      							</div>
      						</div>
      					</div>
      					<div class="title">皮肤选择</div>
      					<div class="setings-item default-skin nb">
      						<span class="skin-name ">
      							<a href="#" class="s-skin-0">
      								默认皮肤
      							</a>
      						</span>
      					</div>
      					<div class="setings-item blue-skin nb">
      						<span class="skin-name ">
      							<a href="#" class="s-skin-1">
      								蓝色主题
      							</a>
      						</span>
      					</div>
      					<div class="setings-item yellow-skin nb">
      						<span class="skin-name ">
      							<a href="#" class="s-skin-3">
      								黄色/紫色主题
      							</a>
      						</span>
      					</div>
      				</div>
      			</div>
      
      		</div>
      
      	</div>
      </div>
      <!--右侧边栏结束-->
       
    </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/hplus.min.js?v=4.1.0"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/contabs.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/plugins/pace/pace.min.js"></script>
    <script>
    </script>
</body>


</html>
