<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
<base href="<%=basePath%>">
<style type="text/css">
	*{margin: 0; padding: 0;}
    .page_top{position: relative; width: 100%; height: 120px; background: #fff;}
    .page_top img{position: absolute; height: 100px; top: 10px; left: 100px;}
    .page_top span{position: absolute; top: 40px; left: 240px; font-size: 24px; color: #00f; letter-spacing: 3px;}

    #top_split{margin: 0px auto; width: 92%; border: none; background: #00f; height: 5px;}
    
    .proto_main{margin: 50px auto; width: 100%; height: 700px;}
    .proto_main .proto_content{position: relative; top: 50px; left: 20%; width: 60%; height: 600px;  background: #eee;}
    .proto_content .proto_title{width: 100%; height: 60px; line-height: 60px; text-align: center;}
    .proto_title span{font-size: 25px; color: #000; letter-spacing: 2px;}
    .proto_content .proto_img{width: 100%; height: 500px; margin-top: 30px; text-align: center;}
    .proto_img img{height: 100%;}
</style>

</head>
<body>
    
    <div class="page_top">
        <img src="image/logo.jpg"/>
        <span>学企新途</span>
    </div>
    <hr id="top_split"/>
    
    <div class='proto_main'>
        <div class='proto_content'>
            <div class='proto_title'>
                <span>${s_email } 同学的就业协议书</span>
            </div>
            <div class='proto_img'>
                <img src='image/${img }'/>
            </div>
        </div>
    </div>
</body>
</html>