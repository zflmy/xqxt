<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    
<!DOCTYPE html PUBLIC>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
<base href="<%=basePath%>">
</head>

<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<style type="text/css">
	*{margin: 0; padding: 0;}
    .page_top{position: relative; width: 100%; height: 120px; background: #fff;}
    .page_top img{position: absolute; height: 100px; top: 10px; left: 100px;}
    .page_top span{position: absolute; top: 40px; left: 240px; font-size: 24px; color: #00f; letter-spacing: 3px;}

    #top_split{margin: 0px auto; width: 92%; border: none; background: #00f; height: 5px;}
    
    .company_main{margin: 50px auto; width:70%; height: 650px; background: #ccc;}
    .company_main .company_title{position: relative; top: 30px; left: 15%; width: 70%; height: 50px; line-height: 50px; text-align: center;}
    .company_title span{font-size: 25px; color: #00f; letter-spacing: 5px; text-shadow: 3px 3px 2px #f00;}
    .company_main .company_content{position: relative; left: 10%; top: 70px; width: 80%; height: 480px;}
    .company_content .company_article{position: relative; top: 20px; left: 20%; width: 60%; height: 440px;}
    .company_article .company_solution{position: relative; top:30px; width: 100%; height: 40px; line-height: 40px;}
    .company_solution span{font-size: 20px; color: #00f; letter-spacing: 3px;}
    .company_article .company_option{position: relative; top: 50px; width: 100%; height: 320px;}
    .company_option .company_answer{width: 100%; height: 260px; line-height: 30px;}
    .company_answer .field{position: relative; top: 30px;width: 450px; height: 30px; border: none; background: #ccc; color: #00f; font-size: 18px; text-align: center; border-bottom: 3px solid #00f;}
    .company_answer textarea{padding: 15px 20px; width: 100%; height: 100%; line-height: 30px; border: none; background: orange; font-size: 18px; color: #00f; border-radius: 5px; letter-spacing: 1px; text-indent: 50px; resize: none;}
    .company_answer .checkbox{position: relative; width: 30px; height: 30px; -moz-appearance: none;}
    .company_answer .checkbox:after{display: inline-block; width: 18px; height: 18px; content: ''; vertical-align: middle; border: 3px solid #00f;}
    .company_answer .checkbox:checked:before{font-size: 23px; position: absolute; top: 0px; left: 3px; content: '√'; color: #f00;}
    .company_answer span{display: inline-block; position: relative; top: 4px; font-size: 18px; color: #00d; letter-spacing: 2px;}
    .company_option .input{display: inline-block; position: relative; top: 4px; border: none; border-bottom: 1px solid #00f; background: #ccc; text-align: center; font-size: 16px; width: 150px; height: 20px; color: #00f;}
    .company_option details{margin: 0px 20px; color: #000;}
    .company_option summary{font-size: 20px; letter-spacing: 3px;}
    .company_option .btn{position: absolute; bottom: 0px; right: 0px; border: none; border-radius: 6px; width: 180px; height: 40px; background: #d00; color: #ddd; font-size: 18px; font-weight: bold;}
    .company_option .btn:hover{cursor: pointer; background: #f00; color: #fff;}
</style>
<body>
    <div class="page_top">
        <img src="image/logo.jpg"/>
        <span></span>
    </div>
    <hr id="top_split"/>
    
    <div class="company_main">
        <div class="company_title">
            <span>企业信息调查问卷</span>
        </div>
        <div class="company_content">
            <div class="company_article">
                <div class="company_solution">
                    <span>1. 请在下列输入框中填写贵公司名称：</span>
                </div>
                <div class="company_option">
                    <div class="company_answer">
                        <input class="field" type="text" name="company_name" placeholder="请输入贵公司的名称..."/> <br/>
                    </div>
                    <button class="btn" onclick="next(2)">下&emsp;一&emsp;步</button>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(function() {
        	
        });
        
        function next(n) {
        	if(n == 2) {
        		$(".company_solution").html("<span>2. 请在下列输入框中填写贵公司简介信息：</span>");
        		$(".company_option").html("<div class='company_answer'>"+
        		        "<textarea name='company_intro' placeholder='请输入贵公司的简介信息...'></textarea>"+
        		    "</div>"+
        		    "<button class='btn' onclick='next(3)''>下&emsp;一&emsp;步</button>"
        		);
        	} else if(n == 3) {
        		$(".company_solution").html("<span>3. 请选择贵公司需招聘的职业类：</span>");
        		$(".company_option").html("<div class='company_answer' style='overflow: hidden scroll;'>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>IT技术类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>管理类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>计算机科学类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>政治行政类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>食品安全类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>计算机硬件类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>网络安全类</span><br/>"+
        				"<input class='checkbox' type='checkbox' name='occupate_no' value=''/>&emsp;<span>人工智能算法类</span><br/>"+
           		    "</div>"+
           		    "<button class='btn' onclick='next(4)''>下&emsp;一&emsp;步</button>"
           		);
        	} else if(n == 4) {
        		$(".company_solution").html("<span>4. 请选择贵公司需招聘的具体职业：</span>");
        		$(".company_option").html("<div class='company_answer' style='overflow: hidden scroll;'>"+
        				"<details>"+
        		            "<summary>IT技术类</summary>"+
        		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>软件工程</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
        		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Java开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
        		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Python开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
        		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>C++开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
        		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>大数据开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
        		        "</details>"+
        		        "<details>"+
    		            "<summary>计算机科学类</summary>"+
		   		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>软件工程</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
		   		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Java开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
		   		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Python开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
		   		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>C++开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
		   		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>大数据开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		        "</details>"+
	    		        "<details>"+
				            "<summary>计算机硬件类</summary>"+
				            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>软件工程</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
				            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Java开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
				            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Python开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
				            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>C++开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
				            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>大数据开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
			        	"</details>"+
			        	"<details>"+
	    		            "<summary>管理类类</summary>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>软件工程</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Java开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Python开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>C++开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>大数据开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		        "</details>"+
	    		        "<details>"+
	    		            "<summary>食品安全类</summary>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>软件工程</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Java开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>Python开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>C++开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		            "<input class='checkbox' type='checkbox' name='job_no' value=''/>&emsp;<span>大数据开发工程师</span>&emsp;&emsp;<input class='input' type='text' name='need_num' placeholder='请输入招聘人数...'/>&emsp;&emsp;<input class='input' type='text' name='salary' placeholder='请输入薪资...'/><br/>"+
	    		        "</details>"+
           		    "</div>"+
           		    "<button class='btn' onclick='next(5)''>下&emsp;一&emsp;步</button>"
           		);
        	} else if(n == 5) {
        		$(".company_solution").html("<span>5. 贵公司是否即将招聘：</span>");
        		$(".company_option").html("<div class='company_answer'>"+
        				"<input class='checkbox' type='radio' name='is_recruit' value='是'/>&emsp;<span>是</span><br/>"+
        				"<input class='checkbox' type='radio' name='is_recruit' value='否'/>&emsp;<span>否</span><br/>"+
           		    "</div>"+
           		    "<button class='btn' onclick='submit()'>下&emsp;一&emsp;步</button>"
           		);
        	}
        }
        
        function submit() {
        	
        }
    </script>
</body>
</html>