<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>我的简历</title>
		<base href="<%=basePath%>">
		
		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/laydate/laydate.css">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">



			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>修改学生信息</h5>

				</div>
				<div class="ibox-content">
					<form class="form-horizontal m-t" id="signupForm" action="insResume" method="post">
						
						<div class="form-group">
							<label class="col-sm-3 control-label">姓名：</label>
							<div class="col-sm-8">
								<input id="s_name" name="s_name" value='' class="form-control inText" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">学号：</label>
							<div class="col-sm-8">
								<input id="s_no" name="s_no" value='' class="form-control inText" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<input type="hidden" name="oldemail" id="oldemail" value="" />
						<div class="form-group">
							<label class="col-sm-3 control-label">邮箱：</label>
							<div class="col-sm-8">
								<input id="email" name="s_email" value='' class="form-control inText" type="email"  disabled="disabled">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">学院：</label>

							<div class="col-sm-8">
								<select class="form-control m-b inText" value=""  id="academy_name"  name="academy_name" required="" aria-required="true">
									<option value =""></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">专业：</label>

							<div class="col-sm-8">
								<select class="form-control m-b inText" value=""  id="major_name"  name="major_name" required="" aria-required="true">
									<option value ="null" selected="true">请先选择学院！</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">班级：</label>

							<div class="col-sm-8">
								<select class="form-control m-b inText" value=""  id="class_name"  name="class_name" required="" aria-required="true">
									<option value ="null" selected="selected">请先选择专业！</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">就业状态：</label>

							<div class="col-sm-8">
								<select class="form-control m-b inText" value=""  id="job_state"  name="job_state" required="" aria-required="true">
									    <option value ="">请选择</option>
                        				<option value ="已就业">已就业</option>
                        				<option value ="考研">考研</option>
                        				<option value ="出国留学">出国留学</option>
                        				<option value ="其他">其他</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3">
								<input type="button" onclick="commit()"  class="btn btn-primary" value="提交" />
							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
		<script src="${pageContext.request.contextPath}/static/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
		<script>
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			//Demo
			$(document).ready(function(){
			}); 
			window.onload = function() { 
    			var $msg = "${msg}";
    			
    			var academy_content="";
    			
				if($msg!=""){
					alert($msg);
					$msg="";
				}
				
				
				$.ajax({
        		url : "selAllAcademy",
        		data : {},
        		dataType : "json",
        		success : function(data) {
        		
        			for(var i = 0; i < data.length; i ++) {
        	        		academy_content += "<option  value='"+data[i].academy_no+"'>"+data[i].academy_name+"</option>"
        	        	}
        	        	$("#academy_name").html(academy_content);
        			
        			},
        			type : "post"
        		});
				
				//var job_state = document.getElementById("job_state_h").value;/* 如果你用的“变量”是input的话，这里就要写成.value了 */
    			//alert(job_state);
    		//	$("#job_state").val(job_state);/* 这句话设置select中value为x的项被选中,例如$("#slt").val(“本科”)就表示<option>本科</option>被选中*/
			
			
			}; 
		$("#academy_name").change(function(){
			var major_content="";
			var select_academy = $(this).children('option:selected').val(); 
			$.ajax({
        	        url : "selAllMajor",
        	        data : {"academy_no":select_academy},
        	        dataType : "json",
        	        success : function(data) {
        	        	for(var i = 0; i < data.length; i ++) {
        	        		major_content += "<option class='stu_major_option_1' value='"+data[i].major_no+"'>"+data[i].major_name+"</option>";
        	        	}
        	        	$("#major_name").html(major_content);
        	        },
        	        type : "post"
        	    });
		});
		$("#major_name").change(function(){
			var clazz_content="";
			var select_major = $(this).children('option:selected').val();
			
			$.ajax({
        		url : "selAllClass",
        		data : {"major_no" : select_major, "academy_no" : $("#academy_name").val()},
        		dataType : "json",
        		success : function(data) {
        			for(var i = 0; i < data.length; i ++) {
        	        		clazz_content += "<option  value='"+data[i].class_name+"'>"+data[i].class_name+"</option>";
        	        	}
        	        	$("#class_name").html(clazz_content);
        			
        			},
        			type : "post"
        		});
		});
		function commit() {
			var s_name=$("#s_name").val();
			var s_no=$("#s_no").val();
			var s_email=$("#email").val();
			var academy_name=$("#academy_name").val();
			var major_name=$("#major_name").val();
			var class_name=$("#class_name").val();
			var job_state=$("#job_state").val();
			$.ajax({
        		url : "admUpdStu",
        		data : {"s_name" : s_name, "s_no" : s_no, "s_email" : s_email, "academy_name" : academy_name, "major_name" : major_name, "class_name" : class_name,"job_state": job_state},
        		dataType : "json",
        		success : function(data) {
        			if(data["result"]=="suess"){
        				layer.msg('修改成功',{time:1*1000},function(){
              			 	 parent.layer.close(index); 
	                	 });
        			}else if(data["result"]=="exception")
        				layer.msg('修改失败',{time:1*1000},function(){
             			 	 parent.layer.close(index); 
	                	 });
        			},
        			type : "post"
        		});
		}
		</script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:16 GMT -->
</html>
