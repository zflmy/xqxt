<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>我的简历</title>
		
		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/laydate/laydate.css">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">



			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>我的简历</h5>

				</div>
				<div class="ibox-content">
					<form class="form-horizontal m-t" id="signupForm" action="insResume" method="post">
						<hr >
							<label class="col-sm-3 " style="color: blue;">基础信息</label><br>
						<hr >
						
						<div class="form-group">
							<label class="col-sm-3 control-label">姓名：</label>
							<div class="col-sm-8">
								<input id="" name="name" value='${resume.name }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">性别：</label>
							<div class="col-sm-8">
								<input id="" name="sex" value='${resume.sex }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">年龄：</label>
							<div class="col-sm-8">
								<input id="" name="age" value='${resume.age }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">学历：</label>
							<div class="col-sm-8">
								<select class="form-control m-b" id="record_edu"  name="record_edu" required="" aria-required="true">
									<option value ="高中">高中</option>
									<option value ="大专">大专</option>
									
									<option value ="本科">本科</option>
									<option value ="硕士">硕士</option>
									<option value ="博士">博士</option>
								</select>

							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">电话：</label>
							<div class="col-sm-8">
								<input id="" name="phone" value='${resume.phone }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<input type="hidden" name="s_email" id="" value="${resume.s_email }" />
						<div class="form-group">
							<label class="col-sm-3 control-label">邮箱：</label>
							<div class="col-sm-8">
								<input id="email" name="" value='${resume.s_email }' class="form-control" type="email" disabled="disabled">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">头像：</label>
							<div class="col-sm-8">
								<input form="form" class="persol_info" type="file" value="" name="head">
							</div>
						</div>
						<hr >
							<label class="col-sm-3 " style="color: blue;">教育背景</label><br>
						<hr >
						<div class="form-group">
							<!-- 注意：这一层元素并不是必须的 -->
							<label class="col-sm-3 control-label">在校时间：</label>
							<div class="col-sm-8">
								<input type="text" name='start_stop_time' value='${resume.start_stop_time }' class="form-control" id="inSchool" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">学校：</label>
							<div class="col-sm-8">
								<input id="" name='school_name' value='${resume.school_name }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">专业：</label>
							<div class="col-sm-8">
								<input id="" name='major' value='${resume.major }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">成绩排名：</label>
							<div class="col-sm-8">
								<input id="" name='score_rank' value='${resume.score_rank }' class="form-control" type="text" required="" aria-required="true" class="valid">
							</div>
						</div>
						<hr >
							<label class="col-sm-3 " style="color: blue;">自我评价</label><br>
						<hr >
						<div class="form-group">
                                <label class="col-sm-3 control-label">专业技能：</label>
                                <div class="col-sm-8">
                                    <textarea id="ccomment" name="skilled" class="form-control" required="" aria-required="true" placeholder="请输入你的专业技能,技术栈等信息,不超过200字">${resume.skilled }</textarea>
                                </div>
                         </div>
						 <div class="form-group">
						         <label class="col-sm-3 control-label">爱好特长：</label>
						         <div class="col-sm-8">
						             <textarea id="ccomment" name="hoppy" class="form-control" required="" aria-required="true" placeholder="请输入爱好特长,不超过200字">${resume.hoppy }</textarea>
						         </div>
						  </div>
						   <div class="form-group">
						          <label class="col-sm-3 control-label">曾获得奖项：</label>
						          <div class="col-sm-8">
						              <textarea id="ccomment" name="prize" class="form-control" required="" aria-required="true" placeholder="请输入你曾经获得的奖项,不超过200字">${resume.prize }</textarea>
						          </div>
						   </div>
						    <div class="form-group">
						          <label class="col-sm-3 control-label">自我评价：</label>
						          <div class="col-sm-8">
						              <textarea id="ccomment" name="self_assessment" class="form-control" required="" aria-required="true" placeholder="请简要评价自己,不超过200字">${resume.self_assessment }</textarea>
						          </div>
						   </div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3">
								<div class="checkbox">
									<label>
										<input type="checkbox" class="checkbox" id="agree" name="agree"> 我已经认真阅读并同意《H+使用协议》
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3">
								<input type="submit"  class="btn btn-primary" value="提交" />
							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
		
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
		
		<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
		<script src="${pageContext.request.contextPath}/static/laydate/laydate.js"></script>
		<script>
			//Demo
			layui.use('form', function() {
				var form = layui.form;
		
				//监听提交
				form.on('submit(formDemo)', function(data) {
					layer.msg(JSON.stringify(data.field));
					return false;
				});
			});
			layui.use('laydate', function() {
				var laydate = layui.laydate;
		
				//执行一个laydate实例
				laydate.render({
					elem: '#stubirthday' //指定元素
				});
		
				laydate.render({
					elem: '#inSchool',
					type: 'month',
					range: true //或 range: '~' 来自定义分割字符
				});
			});
			window.onload = function() { 
    			var $msg = "${msg}";
				if($msg!=""){
					alert($msg);
					$msg="";
				}
				
				obj = document.getElementById("record_edu");
				for(i=0;i<obj.length;i++){
				    if(obj[i].value=="${resume.record_edu}")
				        obj[i].selected = true;
				}
		
			}; 
		</script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:16 GMT -->
</html>
