<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
<base href="<%=basePath%>">

</head>
<link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<style type="text/css">

</style>
<body class="gray-bg">
    <div class="wrapper wrapper-content  animated fadeInRight blog">
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                <c:forEach items='${rcLs}' var='rs'>
                    <div class="ibox-content" style="margin: 10px; border-bottom:1px solid darkgray; ">
                      
                            <h4>
                                                                                    求职者<span style="color: #FF5722;">${rs.s_name}</span>投递了我公司的<span style="color: #FF5722;">${rs.job_name}</span>岗位
                            </h4>
                      
                        <hr >
                       
                        <div class="row" >
              
                            <div class="col-md-6">
                                <div class="small text-right">
                                <!-- interview('${rs.c_email}','${rs.job_no}','${rs.s_email}',3) -->
                                    <h5><a href=javascript:lookAt('${rs.c_email}','${rs.job_no}','${rs.s_email}',2,'${rs.appoint_time}','${rs.appoint_place}')>查看简历</a>&emsp;
                                    <a href=javascript:appintment('${rs.c_email}','${rs.job_no}','${rs.s_email}','${rs.appoint_time}','${rs.appoint_place}')>预约面试</a>&emsp;
                                    <a href=javascript:employ('${rs.c_email}','${rs.job_no}','${rs.s_email}',4)>入职录用</a>&emsp;
                                    <a href=javascript:del('${rs.c_email}','${rs.job_no}','${rs.s_email}')>拒绝并删除</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    </c:forEach>
                </div>
               
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
    <script src="${pageContext.request.contextPath}/static/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/layer/layer.min.js"></script>
	<script>
	window.onload = function() { 
		$("#RC_count",window.parent.document).html("${rcLs_count}");
		
	}
	
		  function lookAt(c_email, job_no, s_email, state, appoint_time, appoint_place){
        	$.ajax({
                url : "updRC",
                data : {"c_email" : c_email, "job_no" : job_no, "s_email" : s_email, "state" : state},
                dataType : "json",
                success : function(data) {
                	location.href="selResume?s_email="+s_email+"&c_email="+c_email+"&job_no="+job_no+"&appoint_time="+appoint_time+"&appoint_place="+appoint_place;
                },
                type : "post"
            });
		  }
		  function interview(c_email, job_no, s_email, state){
	        	$.ajax({
	                url : "updRC",
	                data : {"c_email" : c_email, "job_no" : job_no, "s_email" : s_email, "state" : state},
	                dataType : "json",
	                success : function(data) {
	                	
	                },
	                type : "post"
	            });
	        } 
	        
	        function employ(c_email, job_no, s_email, state) {
	        	$.ajax({
	                url : "updRC",
	                data : {"c_email" : c_email, "job_no" : job_no, "s_email" : s_email, "state" : state},
	                dataType : "json",
	                success : function(data) {
	                	layer.msg("录用成功，请通过邮箱或电话联系他");
	                },
	                type : "post"
	            });
	        }
	        	
	        function del(c_email, job_no, s_email) {
	        	layer.confirm('确定要删除？', {
	        		  btn: ['确定','取消'] //按钮
	        		}, function(){
	        			$.ajax({
	    	                url : "updRC",
	    	                data : {"c_email" : c_email, "job_no" : job_no, "s_email" : s_email, "state" : 5},
	    	                dataType : "json",
	    	                success : function(data) {
	    	                	 layer.msg('删除成功！',{time:1*1000},function(){
	    	                		 location.reload();
	    	                	 	});
	    	                	 
	    	                },
	    	                type : "post"
	    	            });
	        			
	        		}, function(){
	        			location.reload();
	        		});
	        	
	        }
	        
	        function appintment(c_email, job_no, s_email,appoint_time,appoint_place) {
			 	var parentwidth = parseInt(document.body.clientWidth);
			 	var parentheight = parseInt(document.body.clientHeight);
			 	if(parentheight<parseInt(400)){
			 		parentheight=400;
			 	}
			 	var width = parentwidth*0.6+'px';
			 	var height = parentheight*0.6+'px';
			 	
			 	
			 	
        			//alert(tr[0].rowIndex);
        			//alert(td.eq(0).html());
        			layer.open({
                		type: 2,
               		 	shadeClose: true, //点击遮罩关闭层
               		 	area: [width, height],
                		fixed: false, //不固定
                		maxmin: false,
                		closeBtn: 1,
                		resize: true,
                		skin:  'layui-layer-molv',
                		title: "预约面试",
                		content: '${pageContext.request.contextPath}/pages/comInterviewAppointment.jsp',
                 		btn: ['关闭'],
                 			// yes: function(index, layero){
                			//     //按钮【按钮一】的回调
                			//     return false
                			//  },
                			// ,btn2: function(index, layero){
                			//     //按钮【按钮二】的回调
                			//     alert("点击了确认");
                			//     //return false 开启该代码可禁止点击该按钮关闭
                		// },
               		 	success: function(layero, index){
                    		var body = layer.getChildFrame('body',index);//建立父子联系
                    		var iframeWin = window[layero.find('iframe')[0]['name']];
                    		body.find('#appoint_time').val(appoint_time);
                    		body.find('#appoint_place').val(appoint_place);
                    		body.find('#c_email').val(c_email);
                    		body.find('#job_no').val(job_no);
                    		body.find('#s_email').val(s_email);
                		},
                		end: function () {//无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。layer.open关闭事件
                    		location.reload();//layer.open关闭刷新
                			}
            			});
        
  			  			}
        	 
	</script>
</body>

</html>