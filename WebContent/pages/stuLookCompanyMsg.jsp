<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>


<!-- Mirrored from www.zi-han.net/theme/hplus/mail_detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:09 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>公司详情</title>

    <link rel="shortcut icon" href="favicon.ico"> <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">



</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content">
        <div class="row">
            
         
                    
                    <div class="mail-tools tooltip-demo m-t-md">


                        <h3>
                        ${company.company_name }
                    </h3>
                     <div class="head_grade">
               			 <span>${company.grade }分</span>
          			  </div>
                        <h5>
                    </div>
                </div>
				<div class="mail-box">
				    <div class="mail-body">
				        <h4>公司位置规模：</h4>
				        <p>
				   		     公司位于${company.city }；从公司成立截止目前为止人数已经超过${company.have_num }人，并且目前公司还在陆续招聘当中。
				        </p>
						<h4>公司描述：</h4>
						<p>
						${company.company_intro }
						</p>
						<h4>公司招聘职位：</h4>
						<span>
	           	       		     管理类：土地资源管理；资源环境管理；地理科学管理...&emsp;IT技术类：软件工程；Java开发工程师；Python开发工程师；C++开发工程师；大数据研发工程师；数据实施工程师...&emsp;
	                                                       网络安全类：黑客攻防；网络安全；网络工程...
            				</span> 
            			<div style="width: 150px; margin:20px">
            				<a class="btn btn-primary btn-rounded btn-block" href='selOccupateByNo?c_email=${company.c_email }&stu_email=${stu_email}'><i class="fa fa fa-heart"></i> 我想投递</a>	
				        </div>
				        <p class="text-right">
				            ${company.company_name }
				        </p>
				    </div>
				    
				               
				</div>
               
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="${pageContext.request.contextPath}/static/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/mail_detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:10 GMT -->
</html>
>