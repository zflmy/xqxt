<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    
<!DOCTYPE html>
<html>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:15 GMT -->
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<title>校招预约</title>

<base href="<%=basePath%>">

		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/laydate/laydate.css">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight" style="margin: 10%;">

			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>校招预约</h5>

				</div>
				<div class="ibox-content">
					<form class="form-horizontal m-t" id="signupForm" action="insAppoint" method="post">
						<div class="form-group">
							<label class="col-sm-3 control-label">请选择预约时间段</label>
							<div class="col-sm-8">
								<input type="text" name='appoint_time' value='${appointTime}' class="form-control" id="appoint_time" >
								<span class="help-block m-b-none"  id="tishi"></span>
							</div>
						</div>
						<br>
						<hr>
						<br>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-3" style="text-align: center;">
								<input type="button" id="btn_submit" class="btn btn-primary" value="立即预约" />
							</div>
						</div>
					</form>
				</div>
			</div>


		</div>
		<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
		<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
		<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
		
		<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
		<script src="${pageContext.request.contextPath}/static/laydate/laydate.js"></script>
		<script>
			//Demo
			layui.use('form', function() {
				var form = layui.form;
		
				//监听提交
				form.on('submit(formDemo)', function(data) {
					layer.msg(JSON.stringify(data.field));
					return false;
				});
			});
			layui.use('laydate', function() {
				var laydate = layui.laydate;
		
				//执行一个laydate实例
				laydate.render({
					  elem: '#appoint_time'
  						,range: true
				});
				
				
			});
			window.onload = function() { 
    			var $msg = "${msg}";
				if($msg!=""){
					alert($msg);
					$msg="";
				}
				if("${appointTime}"!=""){
					$("#tishi").html("<i class='fa fa-info-circle'></i>您已经预约过，再次预约将覆盖 上一次预约");
				}
			}; 
			
			$("#btn_submit").click(function () {
      	
           	 	var com_email = "${com_email}";
				var  appoint_time= $("#appoint_time").val();
				if("${appointTime}"!=""){
					$.ajax({
                	type: "POST",
                	url: "updAppoint",
                	data: {"com_email":com_email,"appoint_time":appoint_time},
                	async: false,
               	 	success: function(msg){
                   	if(msg["result"]==true){
                    	   alert("预约时间更新成功！");
                    	}else{
                       	 	alert("系统错误");
                   		 }
              		  }
            		});
				}else{
					$.ajax({
                	type: "POST",
                	url: "insAppoint",
                	data: {"com_email":com_email,"appoint_time":appoint_time},
                	async: false,
               	 	success: function(msg){
                   	if(msg["result"]==true){
                    	   alert("预约成功！");
                    	}else{
                       	 	alert("系统错误");
                   		 }
              		  }
            		});
				}
            
        	});
		</script>
	</body>


	<!-- Mirrored from www.zi-han.net/theme/hplus/form_validate.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:19:16 GMT -->
</html>
