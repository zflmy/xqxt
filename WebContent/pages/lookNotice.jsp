<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学企新途</title>
</head>
<link rel="shortcut icon" href="favicon.ico">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<style type="text/css">

</style>
<body class="gray-bg">
    <div class="wrapper wrapper-content  animated fadeInRight blog">
        <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                <c:forEach items='${noticeLs}' var='notice'>
                    <div class="ibox-content" style="margin: 10px; border-bottom:1px solid darkgray; ">
                       
                            <h4>
                                    ${notice.content}
                            </h4>
                        
                      
                        <hr >
                       
                        <div class="row" >
              
                                <div class="small text-right">
                                    <h5>发布于${notice.time}</h5>
                                    <div> <i class="fa fa-comments-o"> </i> 学企新途 </div>
                                </div>
                        </div>
                    </div>
                    
                    </c:forEach>
                </div>
               
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
	<script>
		window.onload = function() { 
    		var $msg = "${msg}";
			if($msg!=""){
				alert($msg);
			}
		}; 
	</script>
</body>

</html>