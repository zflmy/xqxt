
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>企业注册</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="${pageContext.request.contextPath}/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/animate.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body>

<div class="ibox float-e-margins">
    <div class="ibox-title" style="text-align: center">
        <h2>欢迎来到学企新途(企业注册)</h2>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>

        </div>
    </div>
    <div class="ibox-content">
        <form action="register" method="post" class="form-horizontal m-t" id="signupForm" novalidate="novalidate" >
       		 <input type="hidden" name="user_type" value="企业" >
            <div class="form-group">
                <label class="col-sm-3 control-label">E-mail：</label>
                <div class="col-sm-8">
                    <input id="email" name="email" name="email"  class="form-control" type="email">
                    
                    <button class="btn btn-primary" id="email_btn" disabled="disabled"  type="button">发送验证码</button>
                    <span class="help-block m-b-none" id="email_tishi"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">验证码：</label>
                <div class="col-sm-8">
                    <input id="auth_code" name="auth_code" class="form-control" type="text" required="" aria-required="true">
                    <span class="help-block m-b-none" id="auth_code_tishi"><i class="fa fa-info-circle"></i> 请注意查看您的邮箱，若未收到，可能被屏蔽。请到垃圾箱查看（区分大小写）</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">密码：</label>
                <div class="col-sm-8">
                    <input id="password" name="password" class="form-control password_check" type="password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">确认密码：</label>
                <div class="col-sm-8">
                    <input id="confirm_password" name="confirm_password" class="form-control" type="password">
                    <span class="help-block m-b-none" id="passtell"><i class="fa fa-info-circle"></i> 请再次输入您的密码</span>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox" id="agree" name="agree"> 我已经认真阅读并同意<a href="#">《学企新途使用协议》</a>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <button class="btn btn-primary"  type="submit">提交</button>
                    <button class="btn btn-primary" id="back" type="button" onclick=javascrtpt:jump()>返回首页</button>
                </div>
            </div>
            
        </form>
    </div>
</div>

<script src="${pageContext.request.contextPath}/static/js/jquery.min.js?v=2.1.4"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${pageContext.request.contextPath}/static/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/plugins/validate/messages_zh.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/demo/form-validate-demo.min.js"></script>
<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
<script>
	window.onload = function() { 
		if("${err}"!=""){
			alert("${err}");
		}
	}; 
    function jump(){
        window.location.href="${pageContext.request.contextPath}/index.jsp";
    }
    $("#email").focus(function(){
			$("#email_btn").attr('disabled',false);  
	});

	 $(function () {
        $("#email_btn").click(function () {
      	
      	  	$("#email_btn").attr('disabled',true);
            var email = $("#email").val();
			
			
            $.ajax({
                type: "POST",
                url: "registerAuthCode",
                data: {"email":email},
                async: false,
                success: function(msg){
                    if(msg["result"]==true){
                       $("#email_tishi").text("发送成功，请根据邮箱内容填写验证码");
                    }else if(msg["result"]==false){
                        $("#email_tishi").text("发送失败，请检查邮箱是否可用，重新发送");
                    }
                    else if(msg["result"]=="exit"){
                        $("#email_tishi").text("该邮箱已经注册！请返回首页登陆或找回密码");
                    }

                }
            });
        });
    })

</script>

</body>
</html>

