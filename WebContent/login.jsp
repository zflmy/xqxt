<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html >
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>学企新途</title>
  <link href="${pageContext.request.contextPath}/static/css/login.css" rel="stylesheet" rev="stylesheet" type="text/css" media="all" />
  
  
  
  
  <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jQuery1.7.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery1.42.min.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.SuperSlide.js"></script>
   <script src="js/plugins/validate/jquery.validate.min.js"></script>
    
  <script type="text/javascript">
    $(document).ready(function(){

      var $tab_li = $('#tab ul li');
      $tab_li.hover(function(){
        $(this).addClass('selected').siblings().removeClass('selected');
        var index = $tab_li.index(this);
        $('div.tab_box > div').eq(index).show().siblings().hide();
      });
    });
  </script>

  <script type="text/javascript">
  
  
    $(function(){
      $(".screenbg ul li").each(function(){
        $(this).css("opacity","0");
      });
      $(".screenbg ul li:first").css("opacity","1");
      var index = 0;
      var t;
      var li = $(".screenbg ul li");
      var number = li.size();
      function change(index){
        li.css("visibility","visible");
        li.eq(index).siblings().animate({opacity:0},3000);
        li.eq(index).animate({opacity:1},3000);
      }
      function show(){
        index = index + 1;
        if(index<=number-1){
          change(index);
        }else{
          index = 0;
          change(index);
        }
      }
      t = setInterval(show,8000);
      //根据窗口宽度生成图片宽度
      var width = $(window).width();
      $(".screenbg ul img").css("width",width+"px");
    });
    window.onload = function() { 
    	var $msg = "${msg}";
		if($msg!=""){
			alert($msg);
		}
	}; 
  </script>
  <code>
<meta http-equiv="expires" content="0">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
</code>

</head>

<body>
<div id="tab">
  <ul class="tab_menu">
    <li class="selected">学生登录</li>
    <li>企业登录</li>
    <li>管理员登录</li>

  </ul>
  <div class="tab_box">
    <!-- 学生登录开始 -->
    <div>
      <div class="stu_error_box"></div>

      <form action="login" method="post" class="stu_login_error">
        <div id="username">
          <label>账&nbsp;&nbsp;&nbsp;户：</label>
          <input type="hidden" name="user_type" value="学生">
          <input type="text" value="1131233056@qq.com" id="stu_username_hide" name="user_email" placeholder="请输入邮箱" required="" aria-required="true" />
        </div>
        <div id="password">
          <label>密&nbsp;&nbsp;&nbsp;码：</label>
          <input type="password" id="stu_password_hide" value="234234" name="user_pass" placeholder="请输入密码"  required="" aria-required="true" />
        </div>
        <div>
         <a  style="color: blue" href="${pageContext.request.contextPath}/pages/findPass.jsp" >忘记密码？</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a  style="color: blue" href="${pageContext.request.contextPath}/pages/studentZhuce.jsp" >点我注册</a>
        </div>
        <div id="login">
          <button type="submit">登录</button>
        </div>


      </form>

    </div>
    <!-- 学生登录结束-->
    <!-- 企业登录开始-->
    <div class="hide">
      <div class="sec_error_box"></div>
      <form action="login" method="post" class="sec_login_error">
        <div id="username">
          <label>账&nbsp;&nbsp;&nbsp;号：</label>
          <input type="hidden" name="user_type" value="企业">
          <input type="text" id="sec_username_hide" value="albb@163.com" name="user_email" placeholder="请输入邮箱" required="" aria-required="true" />
        </div>
        <div id="password">
          <label>密&nbsp;&nbsp;&nbsp;码：</label>
          <input type="password" id="sec_password_hide" value="123123" name="user_pass" placeholder="请输入密码" required="" aria-required="true" />
        </div>
        <div> 
          <a  style="color: blue" href="${pageContext.request.contextPath}/pages/findPass.jsp" >忘记密码？</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a  style="color: blue" href="${pageContext.request.contextPath}/pages/companyZhuce.jsp" >点我注册</a>
         
        </div>

        <div id="login">
          <button type="submit">登录</button>
        </div>
      </form>
    </div>
    <!-- 教务登录结束-->
    <!-- 管理员登录开始-->
    <div class="hide">
      <div class="tea_error_box"></div>
      <form action="login" method="post" class="tea_login_error">
        <div id="username">
          <label>管理员号：</label>
          <input type="hidden" name="user_type" value="管理员">
          <input type="text" id="email" value="1842327625@qq.com" name="user_email" placeholder="请输入邮箱" required="" aria-required="true" />
        </div>
        <div id="password">
          <label>密&nbsp;&nbsp;&nbsp;码：</label>
          <input type="password" id="tea_password_hide" value="xqxt123" name="user_pass" placeholder="请输入密码"  required="" aria-required="true" />
        </div>
         <div> 
          <a  style="color: blue" href="${pageContext.request.contextPath}/pages/findPass.jsp" >忘记密码？</a>
        </div>
        <div id="login">
          <button type="submit">登录</button>
        </div>
      </form>
    </div>


  </div>
</div>
<div class="screenbg">
  <ul>
    <li><a href="javascript:;"><img src="${pageContext.request.contextPath}/static/images/0.jpg"></a></li>
    <li><a href="javascript:;"><img src="${pageContext.request.contextPath}/static/images/1.jpg"></a></li>
    <li><a href="javascript:;"><img src="${pageContext.request.contextPath}/static/images/2.jpg"></a></li>
  </ul>
</div>
   
</body>
</html>
